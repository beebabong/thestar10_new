package com.gmm.thestar10.APIParse;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.gmm.thestar10.object.Gallery_Detail;
import com.gmm.thestar10.object.Gallerys;
import com.gmm.thestar10.object.News;
import com.gmm.thestar10.object.NewsGallery;
import com.gmm.thestar10.object.VideoHightlight;
import com.gmm.thestar10.object.VideoPart;

public class VideoParseJson {

	 String videoID="videoID";
	 String googleVCode = "googleVCode";
	 String thumbsImg = "thumbsImg";
	 String headline="headline";
	 String caption = "caption";
	 String desc = "desc";
	 String releaseDate = "releaseDate";
	 String lastModifiedDate = "lastModifiedDate";	
	 String videoParts = "videoParts";	
     String linkURL = "linkURL";
	 
	 String videoIDPart = "videoIDPart";
	 String googleVCodePart = "googleVCodePart";
	 String thumbsImgPart = "thumbsImgPart";
	 String headlinePart = "headlinePart";
	 String captionPart = "captionPart";	
	 String descPart = "descPart";
	 String releaseDatePart = "releaseDatePart";	
	 String lastModifiedDatePart = "lastModifiedDatePart";		
    
     List<VideoHightlight> List_VideoHightlight = new ArrayList<VideoHightlight>();
     List<VideoPart>  List_VideoPart ;
	
    
	@SuppressWarnings("static-access")
	public List<VideoHightlight> setvalueInclass(String strJSON){
		try {
			
            JSONObject jObject = new JSONObject(strJSON);
            
            String status = jObject.getString("status");
            String stutusCode = jObject.getString("stutusCode");
            String detail = jObject.getString("detail");
            JSONArray jArray = jObject.getJSONArray("videoHighlights");
            
            Log.i("","status : "+ status); 
            Log.i("","stutusCode : "+ stutusCode);
            Log.i("","detail : "+ detail);
            
           
            for (int i=0; i < jArray.length(); i++){
                    JSONObject oneObject = jArray.getJSONObject(i);
                    List_VideoPart = new ArrayList<VideoPart>();
                    VideoHightlight vdoHL = new VideoHightlight();
                   
                    vdoHL.setCOL_videoID(oneObject.getString(videoID));
                    vdoHL.setCOL_googleVCode(oneObject.getString(googleVCode));
                    vdoHL.setCOL_thumbsImg(oneObject.getString(thumbsImg));
                    vdoHL.setCOL_headline(oneObject.getString(headline));
                    vdoHL.setCOL_caption(oneObject.getString(caption));
                    vdoHL.setCOL_desc(oneObject.getString(desc));
                    vdoHL.setCOL_releaseDate(oneObject.getString(releaseDate));
                    vdoHL.setCOL_desc(oneObject.getString(desc));
                    vdoHL.setCOL_linkURL(oneObject.getString(linkURL));
                    vdoHL.setCOL_lastModifiedDate(oneObject.getString(lastModifiedDate));
                     
                    JSONArray jArray2 = oneObject.getJSONArray(videoParts);
                    for (int j=0; j < jArray2.length(); j++){
                    	JSONObject twoObject = jArray2.getJSONObject(j);
                    	VideoPart vdoPart = new VideoPart();
                    	vdoPart.setCOL_googleVCodePart(twoObject.getString(googleVCodePart));
                    	vdoPart.setCOL_videoIDPart(twoObject.getString(videoIDPart));
                    	vdoPart.setCOL_thumbsImgPart(twoObject.getString(thumbsImgPart));
                    	vdoPart.setCOL_headlinePart(twoObject.getString(headlinePart));
                    	vdoPart.setCOL_captionPart(twoObject.getString(captionPart));
                    	vdoPart.setCOL_descPart(twoObject.getString(descPart));
                    	vdoPart.setCOL_releaseDatePart(twoObject.getString(releaseDatePart));
                    	vdoPart.setCOL_lastModifiedDatePart(twoObject.getString(lastModifiedDatePart));
                    	
                    	List_VideoPart.add(vdoPart);
                    }
                    vdoHL.setCOL_videoParts(List_VideoPart);
                    List_VideoHightlight.add(vdoHL);
            }
            
           
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 return List_VideoHightlight;
	}
	
}
