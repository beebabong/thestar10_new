package com.gmm.thestar10.APIParse;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.gmm.thestar10.object.News;
import com.gmm.thestar10.object.NewsGallery;
import com.gmm.thestar10.object.Login.AccountPayment;
import com.gmm.thestar10.object.Login.accountMSISDN;

public class GetAccountDetailFrommsisdnParseJson {

	String accountID= "accountID";
	 
	String packageID= "packageID";
	String packageName= "packageName";
	String createDate= "createDate";
	String packetStatus= "packetStatus";
	String serviceID= "serviceID";
	
	List<AccountPayment> list_payment ;
     List<accountMSISDN> List_Account = new ArrayList<accountMSISDN>();
     accountMSISDN Account = new accountMSISDN();
    
	@SuppressWarnings("static-access")
	public accountMSISDN setvalueInclass(String strJSON){
		try {
			
            JSONObject jObject = new JSONObject(strJSON);
            
            String status = jObject.getString("status");
            String statusCode = jObject.getString("stutusCode");
            String detail = jObject.getString("detail");
            String accountID = jObject.getString("accountID");
            JSONArray jArray = jObject.getJSONArray("payment");
            
            Log.i("","status : "+ status); 
            Log.i("","stutusCode : "+ statusCode);
            Log.i("","detail : "+ detail);
            
           
            for (int i=0; i < jArray.length(); i++){
                    JSONObject oneObject = jArray.getJSONObject(i);
                    list_payment = new ArrayList<AccountPayment>();
                    AccountPayment account_payment = new AccountPayment();
                   
                    account_payment.setPackageID(oneObject.getInt(packageID));
                    account_payment.setServiceID(oneObject.getInt(serviceID));
                    account_payment.setCreateDate(oneObject.getString(createDate));
                    account_payment.setPackageName(oneObject.getString(packageName));
                    account_payment.setPacketStatus(oneObject.getString(packetStatus));
                    list_payment.add(account_payment);
            }
            
            Account.setStatus(status);
            Account.setDetail(detail);
            Account.setStatusCode(statusCode);
            Account.setAccountID(accountID);
            Account.setPayment(list_payment);
           
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 return Account;
	}
	
}
