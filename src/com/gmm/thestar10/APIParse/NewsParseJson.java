package com.gmm.thestar10.APIParse;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.gmm.thestar10.object.Gallery_Detail;
import com.gmm.thestar10.object.Gallerys;
import com.gmm.thestar10.object.News;
import com.gmm.thestar10.object.NewsGallery;

public class NewsParseJson {

	 String newsID="newsID";
	 String title = "title";
	 String caption = "caption";
	 String thmImg="thmImg";
	 String cover = "cover";
	 String desc = "desc";
	 String releaseDate = "releaseDate";
	 String linkURL = "linkURL";	
	 String newsGallerys = "newsGallerys";	
	 
	 String ID = "ID";
	 String newsGalleryImgURL = "newsGalleryImgURL";
    
     List<News> List_News = new ArrayList<News>();
     List<NewsGallery>  List_NewsGallery ;
	
    
	@SuppressWarnings("static-access")
	public List<News> setvalueInclass(String strJSON){
		try {
			
            JSONObject jObject = new JSONObject(strJSON);
            
            String status = jObject.getString("status");
            String stutusCode = jObject.getString("stutusCode");
            String detail = jObject.getString("detail");
            JSONArray jArray = jObject.getJSONArray("newsList");
            
            Log.i("","status : "+ status); 
            Log.i("","stutusCode : "+ stutusCode);
            Log.i("","detail : "+ detail);
            
           
            for (int i=0; i < jArray.length(); i++){
                    JSONObject oneObject = jArray.getJSONObject(i);
                    List_NewsGallery = new ArrayList<NewsGallery>();
                    News news = new News();
                   
                    news.setCOL_newsID(oneObject.getString(newsID));
                    news.setCOL_title(oneObject.getString(title));
                    news.setCOL_caption(oneObject.getString(caption));
                    news.setCOL_thmImg(oneObject.getString(thmImg));
                    news.setCOL_caption(oneObject.getString(cover));
                    news.setCOL_desc(oneObject.getString(desc));
                    news.setCOL_releaseDate(oneObject.getString(releaseDate));
                    news.setCOL_linkURL(oneObject.getString(linkURL));
                   
//                   
                    JSONArray jArray2 = oneObject.getJSONArray(newsGallerys);
                    for (int j=0; j < jArray2.length(); j++){
                    	JSONObject twoObject = jArray2.getJSONObject(j);
                    	NewsGallery newsgallery = new NewsGallery();
                    	newsgallery.setCOL_ID(twoObject.getString(ID));
                    	newsgallery.setCOL_newsGalleryImgURL(twoObject.getString(newsGalleryImgURL));
                    	
                    	List_NewsGallery.add(newsgallery);
                    }
                    news.setCOL_newsGallerys(List_NewsGallery);
                    List_News.add(news);
            }
            
           
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 return List_News;
	}
	
}
