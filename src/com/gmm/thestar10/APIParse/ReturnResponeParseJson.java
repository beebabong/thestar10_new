package com.gmm.thestar10.APIParse;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.gmm.thestar10.object.Banner;
import com.gmm.thestar10.object.BannerGroup;
import com.gmm.thestar10.object.ReturnRespone;

public class ReturnResponeParseJson {


    ReturnRespone rr = new ReturnRespone();
	@SuppressWarnings("static-access")
	public ReturnRespone setvalueInclass(String strJSON){
		try {
			
            JSONObject jObject = new JSONObject(strJSON);
            
            String status = jObject.getString("status");
            String statusCode = jObject.getString("statusCode");
            String detail = jObject.getString("detail");
           
            rr.setStatus(status);
            rr.setStatusCode(statusCode);
            rr.setDetail(detail);
           
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 return rr;
	}
	
}
