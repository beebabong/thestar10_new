package com.gmm.thestar10.APIParse;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.gmm.thestar10.object.Gallery_Detail;
import com.gmm.thestar10.object.Gallerys;

public class GalleryParseJson {

	String galleryID="galleryID";
	 String thumbsImg = "thumbsImg";
	 String coverImg = "coverImg";
	 String headline="headline";
	 String caption = "caption";
	 String desc = "desc";
	 String releaseDate = "releaseDate";
	 String lastModifiedDate = "lastModifiedDate";	
	 String galleryDetails = "galleryDetails";
	 String galleryDetailID = "galleryDetailID";
     String thumbImgDetail = "thumbImgDetail";
     String coverImgDetail = "coverImgDetail";
     String releaseDateDetail = "releaseDateDetail"; 
     String lastModifiedDateDetail = "lastModifiedDateDetail";
     String linkURL = "linkURL";
    
     List<Gallerys> List_Gallery = new ArrayList<Gallerys>();
     List<Gallery_Detail>  sublistgal_detail ;
	
    
	@SuppressWarnings("static-access")
	public List<Gallerys> setvalueInclass(String strJSON){
		try {
			
            JSONObject jObject = new JSONObject(strJSON);
            
            String status = jObject.getString("status");
            String stutusCode = jObject.getString("stutusCode");
            String detail = jObject.getString("detail");
            JSONArray jArray = jObject.getJSONArray("galleryDTOList");
            
            Log.i("","status : "+ status); 
            Log.i("","stutusCode : "+ stutusCode);
            Log.i("","detail : "+ detail);
            
           
            for (int i=0; i < jArray.length(); i++){
                    JSONObject oneObject = jArray.getJSONObject(i);
                    Gallerys gal = new Gallerys();
                    sublistgal_detail = new ArrayList<Gallery_Detail>();
                    
                    gal.setCOL_galleryID(oneObject.getString(galleryID));
                    gal.setCOL_thumbsImg(oneObject.getString(thumbsImg));
                    gal.setCOL_coverImg(oneObject.getString(coverImg));
                    gal.setCOL_headline(oneObject.getString(headline));
                    gal.setCOL_caption(oneObject.getString(caption));
                    gal.setCOL_desc(oneObject.getString(desc));
                    gal.setCOL_linkURL(oneObject.getString(linkURL));
                    gal.setCOL_releaseDate(oneObject.getString(releaseDate));
                   
//                   
                    JSONArray jArray2 = oneObject.getJSONArray(galleryDetails);
                    for (int j=0; j < jArray2.length(); j++){
                    	JSONObject twoObject = jArray2.getJSONObject(j);
                    	Gallery_Detail subgal_detail = new Gallery_Detail();
                    	subgal_detail.setCOL_galleryDetailID(twoObject.getString(galleryDetailID));
                    	subgal_detail.setCOL_thumbImgDetail(twoObject.getString(thumbImgDetail));
                    	subgal_detail.setCOL_coverImgDetail(twoObject.getString(coverImgDetail));
                    	subgal_detail.setCOL_releaseDateDetail(twoObject.getString(releaseDateDetail));
                    	subgal_detail.setCOL_lastModifiedDateDetail(twoObject.getString(lastModifiedDateDetail));
                    	
                    	sublistgal_detail.add(subgal_detail);
                    }
                    gal.setCOL_galleryDetails(sublistgal_detail);
                    List_Gallery.add(gal);
            }
            
           
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 return List_Gallery;
	}
	
}
