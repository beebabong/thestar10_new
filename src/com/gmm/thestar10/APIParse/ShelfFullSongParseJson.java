package com.gmm.thestar10.APIParse;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.gmm.thestar10.object.Album;
import com.gmm.thestar10.object.Gallery_Detail;
import com.gmm.thestar10.object.Gallerys;
import com.gmm.thestar10.object.shelfFullsong;

public class ShelfFullSongParseJson {

	String status= "status";
	String stutusCode= "stutusCode";
	String detail= "detail";
	String allItem= "allItem";
	String allPage= "allPage";
	String itemPerPage= "itemPerPage";
	String albums= "albums";
	
	String albumID= "albumID";
	String albumName= "albumName";
	String albumArtist= "albumArtist";
	String albumThmCover= "albumThmCover";
	String albumCover= "albumCover";
	String albumYear= "albumYear";
	String albumCPID= "albumCPID";
	
     List<shelfFullsong> List_shelfFullsong = new ArrayList<shelfFullsong>();
     List<Album>  List_album =  new ArrayList<Album>();
    
	@SuppressWarnings("static-access")
	public List<shelfFullsong> setvalueInclass(String strJSON){
		try {
			
            JSONObject jObject = new JSONObject(strJSON);
            shelfFullsong shelfsong =new shelfFullsong();
            
            shelfsong.setStatus(jObject.getString("status"));
            shelfsong.setStatusCode( jObject.getString("stutusCode"));
            shelfsong.setDetail( jObject.getString("detail"));
            shelfsong.setAllItem(jObject.getString(allItem));
            shelfsong.setAllPage(jObject.getString(allPage));
            shelfsong.setItemPerPage( jObject.getString("itemPerPage"));
            
            JSONArray jArray = jObject.getJSONArray(albums);
            
//            Log.i("","status : "+ status); 
//            Log.i("","stutusCode : "+ stutusCode);
//            Log.i("","detail : "+ detail);
            
        	
            for (int i=0; i < jArray.length(); i++){
                    JSONObject oneObject = jArray.getJSONObject(i);
                    Album album = new Album();
                    
                    album.setAlbumID(oneObject.getString(albumID));
                    album.setAlbumcover(oneObject.getString(albumCover));
                    album.setAlbumCPID(oneObject.getString(albumCPID));
                    album.setAlbumName(oneObject.getString(albumName));
                    album.setAlbumThmcover(oneObject.getString(albumThmCover));
                    album.setAlbumYear(oneObject.getString(albumYear));
                    album.setAlbumArtist(oneObject.getString(albumArtist));
                   
                    
                    List_album.add(album);
            }
            shelfsong.setAlbum(List_album);
            List_shelfFullsong.add(shelfsong);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 return List_shelfFullsong;
	}
	
}
