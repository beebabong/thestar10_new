package com.gmm.thestar10.APIParse;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.gmm.thestar10.object.Banner;
import com.gmm.thestar10.object.BannerGroup;

public class BannerParseJson {

	String groupName="groupName";
	 String banner = "banner";
	 
	 String indexPos = "indexPos";
	 String imgURL="imgURL";
	 String desc = "desc";
	 String metaTag = "metaTag";
	
    
     List<BannerGroup> List_bannergroup = new ArrayList<BannerGroup>();
     List<Banner>  List_banner ;
	
    
	@SuppressWarnings("static-access")
	public List<BannerGroup> setvalueInclass(String strJSON){
		try {
			
            JSONObject jObject = new JSONObject(strJSON);
            
            String status = jObject.getString("status");
            String stutusCode = jObject.getString("stutusCode");
            String detail = jObject.getString("detail");
            JSONArray jArray = jObject.getJSONArray("bannerGroups");
            
            Log.i("","status : "+ status); 
            Log.i("","stutusCode : "+ stutusCode);
            Log.i("","detail : "+ detail);
            
           
            for (int i=0; i < jArray.length(); i++){
                    JSONObject oneObject = jArray.getJSONObject(i);
                    List_banner = new ArrayList<Banner>();
                    BannerGroup bann = new BannerGroup();
                    bann.setCOL_groupName(oneObject.getString(groupName));
                   
//                   
                    JSONArray jArray2 = oneObject.getJSONArray(banner);
                    for (int j=0; j < jArray2.length(); j++){
                    	JSONObject twoObject = jArray2.getJSONObject(j);
                    	Banner subgal_detail = new Banner();
                    	subgal_detail.setCOL_indexPos(twoObject.getString(indexPos));
                    	subgal_detail.setCOL_imgURL(twoObject.getString(imgURL));
                    	subgal_detail.setCOL_desc(twoObject.getString(desc));
                    	subgal_detail.setCOL_metaTag(twoObject.getString(metaTag));
                    	
                    	List_banner.add(subgal_detail);
                    }
                    bann.setCOL_banner(List_banner);
                    List_bannergroup.add(bann);
            }
            
           
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 return List_bannergroup;
	}
	
}
