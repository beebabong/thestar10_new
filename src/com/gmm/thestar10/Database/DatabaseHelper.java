package com.gmm.thestar10.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.object.database.Profile;

public class DatabaseHelper extends SQLiteOpenHelper{
	
	

	public static final String DATABASE_NAME = "DB_GMM_AISMUSIC";
	public static final int DATABASE_VERSION = 3;
	
	public static final String TABLE_NAME_PROFILE = "TB_PROFILE";
//	public static final String TABLE_NAME_SONGINPLAYLIST = "TB_SONGINPLAYLIST";
//	public static final String TABLE_NAME_SETTING = "TB_SETTING";
//	public static final String TABLE_NAME_PLAYLIST = "TB_PLAYLIST";
//	public static final String TABLE_NAME_LIBRARY = "TB_LIBRARY";
//	public static final String TABLE_NAME_RADIOLOG = "TB_RADIOLOG";
//	public static final String TABLE_NAME_GMMSESSIONID = "TB_GMMSESSIONID";
	
	//In Version Community U should Use DATABASE_VERSION = 3;
	//------------------------------V.Community ----------------------------------//
	
	private final String SQL_CREATE_TABLE_PROFILE = "create table if not exists "+TABLE_NAME_PROFILE+" ("
														+ Profile.COL_msisdn +" TEXT ,"
														+ Profile.COL_packagetype +" TEXT ,"
														+ Profile.COL_expireddate_member +" TEXT ,"
														+ Profile.COL_expireddate_app +" TEXT ,"
														+ Profile.COL_memberflag +" TEXT);";  // 5 COL


	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.i("OnUpgrade");
		onCreate(db);
		onAlter(db);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_TABLE_PROFILE);
		Log.i("DatabaseHelper - - Oncreate");
	}
	
	public void onAlter(SQLiteDatabase db){
		Log.i("Alter TABLE");
//		if(!existsColumnInTable(db,TABLE_NAME_SETTING,Setting.COL_MODE_STREAM))
//			db.execSQL("ALTER TABLE "+TABLE_NAME_SETTING+" ADD "+Setting.COL_MODE_STREAM+" TEXT DEFAULT 'N'");
//		
//		if(!existsColumnInTable(db,TABLE_NAME_SONGINPLAYLIST,SongInPlaylist.COL_MODE_STREAM))
//			db.execSQL("ALTER TABLE "+TABLE_NAME_SONGINPLAYLIST+" ADD "+SongInPlaylist.COL_MODE_STREAM+" TEXT DEFAULT 'N'");
		
	}

	private boolean existsColumnInTable(SQLiteDatabase inDatabase, String inTable, String columnToCheck) {
	    try{
	        //query 1 row
	        Cursor mCursor  = inDatabase.rawQuery( "SELECT * FROM " + inTable + " LIMIT 0", null );
	        //getColumnIndex gives us the index (0 to ...) of the column - otherwise we get a -1
	        if(mCursor.getColumnIndex(columnToCheck) != -1){
	        	Log.e("Have Field");
	            return true;}
	        else{
	        	Log.e("Not Have Field");
	            return false;
	        }
	    }catch (Exception Exp){
	        //something went wrong. Missing the database? The table?
	        Log.e("... - existsColumnInTable When checking whether a column exists in the table, an error occurred: " + Exp.getMessage());
	        return false;
	    }
	}
}
