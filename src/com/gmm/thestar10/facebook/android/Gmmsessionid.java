package com.gmm.thestar10.facebook.android;

public class Gmmsessionid {

	public static final String COL_contentId = "content_id";
	public static final String COL_gmmSessionId = "gmmsession_id";
	
	private String contentCode;
	private String gmmSessionid;
	
	public String getContentCode() {
		return contentCode;
	}
	public void setContentCode(String contentCode) {
		this.contentCode = contentCode;
	}
	public String getGmmSessionid() {
		return gmmSessionid;
	}
	public void setGmmSessionid(String gmmSessionid) {
		this.gmmSessionid = gmmSessionid;
	}
	
}
