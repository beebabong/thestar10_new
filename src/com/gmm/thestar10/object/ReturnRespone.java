package com.gmm.thestar10.object;

public class ReturnRespone {

	String status;
	String statusCode;
	String detail;
	boolean result;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	
	
	public boolean isResult() {
		if(status.equals("200")) return result;
		return false;
		
	}
	
}
