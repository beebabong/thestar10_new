package com.gmm.thestar10.object;

import java.util.List;

public class shelfFullsong {

	String status;
	String statusCode;
	String detail;
	String allItem;
	String allPage;
	String itemPerPage;
	List<Album> album;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getAllItem() {
		return allItem;
	}
	public void setAllItem(String allItem) {
		this.allItem = allItem;
	}
	public String getAllPage() {
		return allPage;
	}
	public void setAllPage(String allPage) {
		this.allPage = allPage;
	}
	public String getItemPerPage() {
		return itemPerPage;
	}
	public void setItemPerPage(String itemPerPage) {
		this.itemPerPage = itemPerPage;
	}
	public List<Album> getAlbum() {
		return album;
	}
	public void setAlbum(List<Album> album) {
		this.album = album;
	}
	
}
