package com.gmm.thestar10.object;

public class Gallery_Detail {

	String COL_galleryDetailID;
	String COL_thumbImgDetail;
    String COL_coverImgDetail;
    String COL_releaseDateDetail; 
    String COL_lastModifiedDateDetail;
	
	
	public String getCOL_galleryDetailID() {
		return COL_galleryDetailID;
	}
	public void setCOL_galleryDetailID(String cOL_galleryDetailID) {
		COL_galleryDetailID = cOL_galleryDetailID;
	}
	public String getCOL_thumbImgDetail() {
		return COL_thumbImgDetail;
	}
	public void setCOL_thumbImgDetail(String cOL_thumbImgDetail) {
		COL_thumbImgDetail = cOL_thumbImgDetail;
	}
	public String getCOL_coverImgDetail() {
		return COL_coverImgDetail;
	}
	public void setCOL_coverImgDetail(String cOL_coverImgDetail) {
		COL_coverImgDetail = cOL_coverImgDetail;
	}
	public String getCOL_releaseDateDetail() {
		return COL_releaseDateDetail;
	}
	public void setCOL_releaseDateDetail(String cOL_releaseDateDetail) {
		COL_releaseDateDetail = cOL_releaseDateDetail;
	}
	public String getCOL_lastModifiedDateDetail() {
		return COL_lastModifiedDateDetail;
	}
	public void setCOL_lastModifiedDateDetail(String cOL_lastModifiedDateDetail) {
		COL_lastModifiedDateDetail = cOL_lastModifiedDateDetail;
	}
		
	    
	    
	    
}
