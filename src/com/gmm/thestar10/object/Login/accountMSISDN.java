package com.gmm.thestar10.object.Login;

import java.util.List;

public class accountMSISDN {

	String status;
	String statusCode;
	String detail;
	String accountID;
	List<AccountPayment> payment;

	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getAccountID() {
		return accountID;
	}
	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}
	public List<AccountPayment> getPayment() {
		return payment;
	}
	public void setPayment(List<AccountPayment> payment) {
		this.payment = payment;
	}
		
}
