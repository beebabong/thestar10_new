package com.gmm.thestar10.object.Login;

public class AccountPayment {

	int packageID;
	String packageName;
	String createDate;
	String packetStatus;
	int serviceID;
	
	public int getPackageID() {
		return packageID;
	}
	public void setPackageID(int packageID) {
		this.packageID = packageID;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getPacketStatus() {
		return packetStatus;
	}
	public void setPacketStatus(String packetStatus) {
		this.packetStatus = packetStatus;
	}
	public int getServiceID() {
		return serviceID;
	}
	public void setServiceID(int serviceID) {
		this.serviceID = serviceID;
	}
	
}
