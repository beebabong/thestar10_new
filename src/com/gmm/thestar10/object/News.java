package com.gmm.thestar10.object;

import java.util.List;

public class News {
	 String COL_newsID;
	 String COL_title;
	 String COL_caption;
	 String COL_thmImg;
	 String COL_cover;
	 String COL_desc;
	 String COL_releaseDate;
	 String COL_linkURL;
	 List<NewsGallery> COL_newsGallerys;	
	 
	 
	 public List<NewsGallery> getCOL_newsGallerys() {
		return COL_newsGallerys;
	}
	public void setCOL_newsGallerys(List<NewsGallery> cOL_newsGallerys) {
		COL_newsGallerys = cOL_newsGallerys;
	}
	 public String getCOL_newsID() {
		return COL_newsID;
	}
	public void setCOL_newsID(String cOL_newsID) {
		COL_newsID = cOL_newsID;
	}
	public String getCOL_title() {
		return COL_title;
	}
	public void setCOL_title(String cOL_title) {
		COL_title = cOL_title;
	}
	public String getCOL_caption() {
		return COL_caption;
	}
	public void setCOL_caption(String cOL_caption) {
		COL_caption = cOL_caption;
	}
	public String getCOL_thmImg() {
		return COL_thmImg;
	}
	public void setCOL_thmImg(String cOL_thmImg) {
		COL_thmImg = cOL_thmImg;
	}
	public String getCOL_cover() {
		return COL_cover;
	}
	public void setCOL_cover(String cOL_cover) {
		COL_cover = cOL_cover;
	}
	public String getCOL_desc() {
		return COL_desc;
	}
	public void setCOL_desc(String cOL_desc) {
		COL_desc = cOL_desc;
	}
	public String getCOL_releaseDate() {
		return COL_releaseDate;
	}
	public void setCOL_releaseDate(String cOL_releaseDate) {
		COL_releaseDate = cOL_releaseDate;
	}
	public String getCOL_linkURL() {
		return COL_linkURL;
	}
	public void setCOL_linkURL(String cOL_linkURL) {
		COL_linkURL = cOL_linkURL;
	}
	
	 }
