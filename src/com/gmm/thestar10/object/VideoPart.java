package com.gmm.thestar10.object;

public class VideoPart {
	 String COL_googleVCodePart;
	 String COL_thumbsImgPart;
	 String COL_headlinePart;
	 String COL_captionPart;	
	 String COL_descPart;
	 String COL_releaseDatePart;	
	 String COL_lastModifiedDatePart;	
	 
	 
	String COL_videoIDPart;
	 public String getCOL_videoIDPart() {
		return COL_videoIDPart;
	}
	public void setCOL_videoIDPart(String cOL_videoIDPart) {
		COL_videoIDPart = cOL_videoIDPart;
	}
	public String getCOL_googleVCodePart() {
		return COL_googleVCodePart;
	}
	public void setCOL_googleVCodePart(String cOL_googleVCodePart) {
		COL_googleVCodePart = cOL_googleVCodePart;
	}
	public String getCOL_thumbsImgPart() {
		return COL_thumbsImgPart;
	}
	public void setCOL_thumbsImgPart(String cOL_thumbsImgPart) {
		COL_thumbsImgPart = cOL_thumbsImgPart;
	}
	public String getCOL_headlinePart() {
		return COL_headlinePart;
	}
	public void setCOL_headlinePart(String cOL_headlinePart) {
		COL_headlinePart = cOL_headlinePart;
	}
	public String getCOL_captionPart() {
		return COL_captionPart;
	}
	public void setCOL_captionPart(String cOL_captionPart) {
		COL_captionPart = cOL_captionPart;
	}
	public String getCOL_descPart() {
		return COL_descPart;
	}
	public void setCOL_descPart(String cOL_descPart) {
		COL_descPart = cOL_descPart;
	}
	public String getCOL_releaseDatePart() {
		return COL_releaseDatePart;
	}
	public void setCOL_releaseDatePart(String cOL_releaseDatePart) {
		COL_releaseDatePart = cOL_releaseDatePart;
	}
	public String getCOL_lastModifiedDatePart() {
		return COL_lastModifiedDatePart;
	}
	public void setCOL_lastModifiedDatePart(String cOL_lastModifiedDatePart) {
		COL_lastModifiedDatePart = cOL_lastModifiedDatePart;
	}
	
}
