package com.gmm.thestar10.object;

public class Album {

	String albumID;
	String albumName;
	String albumArtist;
	String albumThmcover;
	String albumcover;
	String albumYear;
	String albumCPID;
	
	public String getAlbumID() {
		return albumID;
	}
	public void setAlbumID(String albumID) {
		this.albumID = albumID;
	}
	public String getAlbumName() {
		return albumName;
	}
	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}
	public String getAlbumArtist() {
		return albumArtist;
	}
	public void setAlbumArtist(String alvumArtist) {
		this.albumArtist = alvumArtist;
	}
	public String getAlbumThmcover() {
		return albumThmcover;
	}
	public void setAlbumThmcover(String albumThmcover) {
		this.albumThmcover = albumThmcover;
	}
	public String getAlbumcover() {
		return albumcover;
	}
	public void setAlbumcover(String albumcover) {
		this.albumcover = albumcover;
	}
	public String getAlbumYear() {
		return albumYear;
	}
	public void setAlbumYear(String albumYear) {
		this.albumYear = albumYear;
	}
	public String getAlbumCPID() {
		return albumCPID;
	}
	public void setAlbumCPID(String albumCPID) {
		this.albumCPID = albumCPID;
	}
	
	
}
