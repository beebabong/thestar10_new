package com.gmm.thestar10.object;

import java.util.ArrayList;
import java.util.List;

public class Gallerys {
	 	
	 	 String COL_headline ;
	 	 String COL_galleryID ;		
	 	 String COL_thumbsImg;
		 String COL_coverImg ;
		 String COL_caption;
		 String COL_desc;
		 String COL_releaseDate;
		 String COL_lastModifiedDate ;
		 String COL_linkURL;
		 List<Gallery_Detail> COL_galleryDetails;
			 
			 public String getCOL_linkURL() {
				return COL_linkURL;
			}
			public void setCOL_linkURL(String cOL_linkURL) {
				COL_linkURL = cOL_linkURL;
			}
		 
		 
		 public String getCOL_headline() {
			return COL_headline;
		}
		public void setCOL_headline(String cOL_headline) {
			COL_headline = cOL_headline;
		}
		public String getCOL_galleryID() {
			return COL_galleryID;
		}
		public void setCOL_galleryID(String cOL_galleryID) {
			COL_galleryID = cOL_galleryID;
		}
		public String getCOL_thumbsImg() {
			return COL_thumbsImg;
		}
		public void setCOL_thumbsImg(String cOL_thumbsImg) {
			COL_thumbsImg = cOL_thumbsImg;
		}
		public String getCOL_coverImg() {
			return COL_coverImg;
		}
		public void setCOL_coverImg(String cOL_coverImg) {
			COL_coverImg = cOL_coverImg;
		}
		public String getCOL_caption() {
			return COL_caption;
		}
		public void setCOL_caption(String cOL_caption) {
			COL_caption = cOL_caption;
		}
		public String getCOL_desc() {
			return COL_desc;
		}
		public void setCOL_desc(String cOL_desc) {
			COL_desc = cOL_desc;
		}
		public String getCOL_releaseDate() {
			return COL_releaseDate;
		}
		public void setCOL_releaseDate(String cOL_releaseDate) {
			COL_releaseDate = cOL_releaseDate;
		}
		public String getCOL_lastModifiedDate() {
			return COL_lastModifiedDate;
		}
		public void setCOL_lastModifiedDate(String cOL_lastModifiedDate) {
			COL_lastModifiedDate = cOL_lastModifiedDate;
		}
		public List<Gallery_Detail> getCOL_galleryDetails() {
			return COL_galleryDetails;
		}
		public void setCOL_galleryDetails(List<Gallery_Detail> cOL_galleryDetails) {
			COL_galleryDetails = cOL_galleryDetails;
		}
		
}
