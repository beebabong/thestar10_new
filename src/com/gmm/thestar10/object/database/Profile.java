package com.gmm.thestar10.object.database;

import java.util.Calendar;


public class Profile {
	public static final String COL_msisdn="msisdn";
	public static final String COL_packagetype="packagetype";
	public static final String COL_expireddate_member="expireddate_member";
	public static final String COL_expireddate_app = "expireddate_app";
	public static final String COL_memberflag="memberflage";
	
	private String msisdn="";
	private String packageType = "";
	private String expireddateMember="";
	private String expireddateApp="";
	private String memberFlag="";
	
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getPackageType() {
		return packageType;
	}
	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}
	public String getExpireddateMember() {
		return expireddateMember;
	}
	public void setExpireddateMember(String expireddateMember) {
		this.expireddateMember = expireddateMember;
	}
	public String getExpireddateApp() {
		return expireddateApp;
	}
	public void setExpireddateApp(String expireddateApp) {
		this.expireddateApp = expireddateApp;
	}
	public String getMemberFlag() {
		return memberFlag;
	}
	public boolean isMemberFlag() {
		return (memberFlag.equalsIgnoreCase("Y")? true : false);
	}
	public void setMemberFlag(String memberFlag) {
		this.memberFlag = memberFlag;
	}
	public  String getCurrentCalendar7Day() {
		// set Calendate Now
		final Calendar c = Calendar.getInstance();
		return  (c.get(Calendar.YEAR) + "" + pad(c.get(Calendar.MONTH) + 1) + pad(c.get(Calendar.DAY_OF_MONTH) + 7));
	}
	private static String pad(int c) {
		if (c >= 10) {
			return String.valueOf(c);
		} else {
			return "0" + String.valueOf(c);
		}
	}
}
