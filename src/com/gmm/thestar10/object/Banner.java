package com.gmm.thestar10.object;

public class Banner {
	 String COL_indexPos;
	 String COL_imgURL;
	 String COL_desc ;
	 String COL_metaTag ;
	 
	 public String getCOL_indexPos() {
		return COL_indexPos;
	}
	public void setCOL_indexPos(String cOL_indexPos) {
		COL_indexPos = cOL_indexPos;
	}
	public String getCOL_imgURL() {
		return COL_imgURL;
	}
	public void setCOL_imgURL(String cOL_imgURL) {
		COL_imgURL = cOL_imgURL;
	}
	public String getCOL_desc() {
		return COL_desc;
	}
	public void setCOL_desc(String cOL_desc) {
		COL_desc = cOL_desc;
	}
	public String getCOL_metaTag() {
		return COL_metaTag;
	}
	public void setCOL_metaTag(String cOL_metaTag) {
		COL_metaTag = cOL_metaTag;
	}
	
}
