package com.gmm.thestar10;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gmm.thestar10.R;

public class menu_Adapter extends ArrayAdapter<String> {
	  Context context;
//	  int[] icon ={R.drawable.icon_vote,R.drawable.icon_thestar_profile,R.drawable.icon_new,
//			  R.drawable.icon_vdo,R.drawable.icon_gellery};
//	  
//	  String[] text_list = {"Vote","The Start Profile","News","Video","Gallery","Download",
//			  "Song / MV","Say Hi! / Spacial clips","Games","Start Frame","Star Quiz","My Downloaded"};
	  
	  int[] icon ={R.drawable.icon_new,R.drawable.icon_vdo,R.drawable.icon_gellery,R.drawable.icon_setting,R.drawable.icon_downloaded};

	  String[] text_list = {"News","Video","Gallery","Setting","Download Song/MV"};
	  int select;
	  static class ViewHolder {
		 static ImageView icon;
		 static TextView txt_listmenu,txt_titlemenu;
		 static LinearLayout layout_list;
		}
	  
	  public menu_Adapter(Context context,int select) {
		  super(context, R.layout.ac_menu);
		  this.context = context;
		  this.select = select;
	  }
	  
	  @Override
	  public int getCount() {
		return text_list.length;
	  }
	  
	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView;
	    
	    rowView = inflater.inflate(R.layout.ac_menu, parent, false);
   	 	ViewHolder.icon = (ImageView) rowView.findViewById(R.id.icon_menu);
   	 	ViewHolder.txt_listmenu = (TextView) rowView.findViewById(R.id.txt_listmenu);
   	 	ViewHolder.layout_list = (LinearLayout) rowView.findViewById(R.id.layout_list);
   	 
   	 	ViewHolder.icon.setImageResource(icon[position]);
   	 	ViewHolder.txt_listmenu.setText(text_list[position]) ;
   	 
   	 if(position == select) ViewHolder.layout_list.setBackgroundColor(R.color.blue);
	    
//	    if(position!=5 && position !=8){
//	    	 rowView = inflater.inflate(R.layout.ac_menu, parent, false);
//	    	 ViewHolder.icon = (ImageView) rowView.findViewById(R.id.icon_menu);
//	    	 ViewHolder.txt_listmenu = (TextView) rowView.findViewById(R.id.txt_listmenu);
//	    	 ViewHolder.layout_list = (LinearLayout) rowView.findViewById(R.id.layout_list);
//	    	 
//	    	 if(position<5) ViewHolder.icon.setImageResource(icon[position]);
//	    	 else if(position == 6 || position == 7) ViewHolder.icon.setImageResource(R.drawable.icon_downloaded);
//	    	 else if(position == 9 || position == 10) ViewHolder.icon.setImageResource(R.drawable.icon_game);
//	    	 else ViewHolder.icon.setImageResource(R.drawable.icon_mydownloaded);
//	    	 
//	    	 ViewHolder.txt_listmenu.setText(text_list[position]) ;
//	    	 
//	    	 if(position == select) ViewHolder.layout_list.setBackgroundColor(R.color.grey_soft);
//	    }
//	    else{
//	    	 rowView = inflater.inflate(R.layout.ac_title_menu, parent, false);
//	    	 ViewHolder.txt_titlemenu = (TextView) rowView.findViewById(R.id.txt_titlelistmenu);
//	    	 ViewHolder.txt_titlemenu.setText(text_list[position]) ;
//	    }
	   
	    return rowView;
	  }
	  
	  
	} 

