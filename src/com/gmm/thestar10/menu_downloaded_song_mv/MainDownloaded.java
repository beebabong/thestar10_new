package com.gmm.thestar10.menu_downloaded_song_mv;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.LocalActivityManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.ViewFlipper;

import com.gmm.thestar10.R;
import com.gmm.thestar10.menu_Adapter;
import com.gmm.thestar10.API.BannerAPI;
import com.gmm.thestar10.ApplicationConfig.BannerAdapter;
import com.gmm.thestar10.ApplicationConfig.GroupMainActivity;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.Untilitys.Unsteady;
import com.gmm.thestar10.flyoutmenu.FlyOutContainer;
import com.gmm.thestar10.menu_gallery.Gallery_Activity;
import com.gmm.thestar10.menu_news.News_Activity;
import com.gmm.thestar10.menu_news.News_Activity.DownloadImageTask;
import com.gmm.thestar10.menu_setting.Setting_Activity;
import com.gmm.thestar10.menu_video.Video_Activity;
import com.gmm.thestar10.object.Banner;
import com.gmm.thestar10.object.BannerGroup;

public class MainDownloaded extends Activity implements OnTouchListener{
	TabHost mTabHost; 
	LocalActivityManager myLocalActivityManager;
	// menu
			FlyOutContainer root;
			ListView listview_main_menu;
			ImageView btn_menu;
	
	//Activity in this
			ViewFlipper viewflipBanner;
			LinearLayout layout_main;
			
	float downX = 0, downY = 0, upX, upY;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.ac_downloaded);
		this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.ac_downloaded, null);
		this.setContentView(root);
		
		
		mTabHost = (TabHost) root.findViewById(android.R.id.tabhost);
		viewflipBanner = (ViewFlipper) root.findViewById(R.id.viewflipBanner);
		layout_main = (LinearLayout) root.findViewById(R.id.layout_main);
		
		if(Unsteady.List_BannerGroup.size()==0) Unsteady.List_BannerGroup = new BannerAPI().getJSONFromUrl();
		new DownloadImageTask().execute();
    	
		layout_main.setOnTouchListener(this);
        
		myLocalActivityManager = new LocalActivityManager(this, false);
    	myLocalActivityManager.dispatchCreate(savedInstanceState);
		
        mTabHost.setup(myLocalActivityManager);
       
        
        Intent Song_Downloaded = new Intent().setClass(MainDownloaded.this, Song_Downloaded.class);
        TabHost.TabSpec specsong = mTabHost
        		.newTabSpec("tabSong")
        		.setIndicator("SONG")
        		.setContent(Song_Downloaded);
        mTabHost.addTab(specsong);
        
        Intent VDO_Downloaded = new Intent().setClass(MainDownloaded.this, VDO_Downloaded.class);
        TabHost.TabSpec specvdo = mTabHost
        		.newTabSpec("tabVDO")
        		.setIndicator("VDO")
        		.setContent(VDO_Downloaded);
        mTabHost.addTab(specvdo);
        
        mTabHost.setCurrentTab(0);
        
        setmenu();
			
	}
	
	//******************************************************************************************************************************************************
		//********************************************************************SET MENU**************************************************************************
		//******************************************************************************************************************************************************
		
		private void setmenu() {
			listview_main_menu = (ListView) root.findViewById(R.id.listview_main_menu);
			btn_menu = (ImageView) root.findViewById(R.id.btn_menu);
			
			listview_main_menu.setAdapter(new menu_Adapter(this,4));

			btn_menu.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					root. toggleMenu();
					
				}
			});
			listview_main_menu.setOnItemClickListener(new OnItemClickListener() {

				@SuppressWarnings("deprecation")
				@Override
				public void onItemClick(AdapterView<?> arg0, View v, int pos,
						long arg3) {
					Log.i("click :"+pos);
					 
					switch (pos) {

					case 0: startActivity(new Intent(MainDownloaded.this,News_Activity.class)); break;
			        case 1: startActivity(new Intent(MainDownloaded.this,Video_Activity.class)); break;
					case 2: startActivity(new Intent(MainDownloaded.this,Gallery_Activity.class)); break;
			        case 3: startActivity(new Intent(MainDownloaded.this,Setting_Activity.class)); break;
			

					}   
					
				}
			});
		}

		private void insertDecorView(View view) {
			getParent().setContentView(
					view,
					new LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.FILL_PARENT));
		}

		
		//******************************************************************************************************************************************************
		//************************************************************ SET CLICK / TOUCH ***********************************************************************
		//******************************************************************************************************************************************************

		@Override
		public boolean onTouch(View v, MotionEvent event) {

			int MIN_DISTANCE = 100;
			if(downX==0) downX = event.getX();
			if(downY==0) downY = event.getY();
			
			 switch(event.getAction()){
			 
		        case MotionEvent.ACTION_UP: {
		            upX = event.getX();
		            upY = event.getY();

		            float deltaX = downX - upX;

		            if(Math.abs(deltaX) > MIN_DISTANCE){
		                // left or right
		                if(deltaX < 0 || deltaX > 0) { root. toggleMenu(); }
		            }
		           
		            return true;
		        }
		    }
		    return false;
		}
		
		//******************************************************************************************************************************************************
		//********************************************************************SET BANNER************************************************************************
		//******************************************************************************************************************************************************
				
		
		public class DownloadImageTask extends AsyncTask<String, Void, Void> {
			protected Void doInBackground(String... urls) {
				try {
					if(Unsteady.bannerBitmap.size()==0){
						Unsteady.bannerBitmap = new ArrayList<Bitmap>();
						if (Unsteady.List_BannerGroup.size() != 0) {
							for (BannerGroup item : Unsteady.List_BannerGroup) {
								for(Banner bann : item.getCOL_banner()){
									URL url = new URL(bann.getCOL_imgURL());
									
							        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
							        connection.setDoInput(true);
							        connection.connect();
							        InputStream input = connection.getInputStream();
							        Bitmap myBitmap = BitmapFactory.decodeStream(input);

									Unsteady.bannerBitmap.add(myBitmap);
								}
								
							}
						}else{
							Log.i("Banner : DownloadImageTask == Null");
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				if (Unsteady.bannerBitmap.size() != 0) {
					new BannerAdapter(MainDownloaded.this, viewflipBanner);
				}
			}
		}
		//******************************************************************************************************************************************************
		//******************************************************************* EXIT *****************************************************************************
		//******************************************************************************************************************************************************

		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			// TODO Auto-generated method stub
			if (keyCode == KeyEvent.KEYCODE_BACK ){
				exit();
			}
			
			return super.onKeyDown(keyCode, event);
		}
		
		public void exit(){
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Exit ?");
		
			builder.setMessage("คุณต้องการออกจากแอพพลิเคชั่น ")
			       .setCancelable(true)
			       
			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
							Intent startMain = new Intent(
									Intent.ACTION_MAIN);
							startMain.addCategory(Intent.CATEGORY_HOME);
							startMain
									.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(startMain);
							onDestroy();
							android.os.Process
									.killProcess(android.os.Process.myPid());
			           }
			       })
			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			                dialog.cancel();
			           }
			       });
			 builder.show();
		}
		
}
