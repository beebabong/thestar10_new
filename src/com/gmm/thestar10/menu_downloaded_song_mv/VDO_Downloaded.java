package com.gmm.thestar10.menu_downloaded_song_mv;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gmm.thestar10.R;
import com.gmm.thestar10.API.BannerAPI;
import com.gmm.thestar10.API.ShelfMVAPI;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.Untilitys.Unsteady;
import com.gmm.thestar10.menu_downloaded_song_mv.Song_Downloaded.ViewHolder;
import com.gmm.thestar10.object.Album;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class VDO_Downloaded extends Activity implements Runnable{

	ListView listview;

    DisplayImageOptions options;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listview);
		
		listview = (ListView) findViewById(R.id.listView);
	        
		new Thread(this).start();
	}
	//******************************************************************************************************************************************************
	//****************************************************************SET RUNNABLE**************************************************************************
	//******************************************************************************************************************************************************
	
	
	@Override
	public void run() {																								// RUN
		if(Unsteady.List_BannerGroup.size()==0) Unsteady.List_BannerGroup = new BannerAPI().getJSONFromUrl();
		if(Unsteady.shelf_MV.size()==0) Unsteady.shelf_MV = new ShelfMVAPI().getJSONFromUrl();
		
			for(Album a : Unsteady.shelf_MV.get(0).getAlbum()){
				Log.i("shelf full song : "+a.getAlbumName());
			}
		
		handler.sendEmptyMessage(0);	
		
	}
	
	 private Handler handler = new Handler() {																		// HANDLER
		    @Override
		    
		    public void handleMessage(Message msg) {

				listview.setAdapter(new SongPlaylistAdapter(VDO_Downloaded.this.getParent()));
		    	
		    }
	 };
	
	static class ViewHolder {
		  static ImageView img_cover ;
		  static TextView txt_songname,txt_albumname,txt_artistname,txt_track;
		}
	
	public class SongPlaylistAdapter extends ArrayAdapter<String> {
		  Context context;
			DisplayImageOptions options;
			protected ImageLoader imageLoader = ImageLoader.getInstance();
		 
		  
		  public SongPlaylistAdapter(Context context) {
			  super(context, R.layout.inlist_download);
			  this.context = context;
			  
			  options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.icon_cover)
				.showImageForEmptyUri(R.drawable.icon_cover)
				.showImageOnFail(R.drawable.icon_cover).cacheInMemory()
				.cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).build();
			  imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		  }
		  
		
		  
		  @Override
		  public int getCount() {
			  return Unsteady.shelf_MV.get(0).getAlbum().size();
		  }
		  
		  @Override
		  public View getView(final int position, View convertView, ViewGroup parent) {
		    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    View rowView;
		    	rowView = inflater.inflate(R.layout.inlist_download, parent, false);
		    	ViewHolder. img_cover = (ImageView)rowView.findViewById(R.id.img_cover);
			    ViewHolder.txt_songname = (TextView)rowView.findViewById(R.id.txt_songname);
			    ViewHolder.txt_albumname = (TextView)rowView.findViewById(R.id.txt_albumname);
			    ViewHolder.txt_artistname = (TextView)rowView.findViewById(R.id.txt_artistname);
			    ViewHolder.txt_track = (TextView)rowView.findViewById(R.id.txt_track);

			    LinearLayout.LayoutParams flp= new LinearLayout.LayoutParams(200, 200);
				flp.setMargins(2, 2, 2, 2);
				ViewHolder. img_cover.setLayoutParams(flp);
				
			    ViewHolder.txt_songname .setText("รวม MV ");
			    ViewHolder.txt_albumname .setText(Unsteady.shelf_MV.get(0).getAlbum().get(position).getAlbumName());
			    ViewHolder.txt_artistname .setText("ศิลปิน"+Unsteady.shelf_MV.get(0).getAlbum().get(position).getAlbumArtist());

				imageLoader.displayImage(Unsteady.shelf_MV.get(0).getAlbum().get(position).getAlbumcover(), ViewHolder. img_cover, options);
			
		    
		    return rowView;
		  }
	}

}
