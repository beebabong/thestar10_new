package com.gmm.thestar10;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.LinearLayout.LayoutParams;

import com.gmm.thestar10.API.BannerAPI;
import com.gmm.thestar10.ApplicationConfig.DeviceInfo;
import com.gmm.thestar10.ApplicationConfig.GroupMainActivity;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.Untilitys.Unsteady;
import com.gmm.thestar10.menu_news.News_Activity;
import com.gmm.thestar10.object.Banner;
import com.gmm.thestar10.object.BannerGroup;

public class Main_theStar10 extends Activity implements Runnable{
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 
        setContentView(R.layout.activity_main_the_start10);
 
//        new Thread(this).start();
        Handler handler1 = new Handler(); 
        handler1.postDelayed(this, 3000);
        DeviceInfo.getDeviceID(getApplicationContext());
        Unsteady.flageInternet = DeviceInfo.isCheckInternet(getApplicationContext());
       
		
    }

	@Override
	public void run() {
		 
		 Unsteady.List_BannerGroup = new BannerAPI().getJSONFromUrl();
		 
		 try {
				Unsteady.bannerBitmap = new ArrayList<Bitmap>();
				if (Unsteady.List_BannerGroup.size() != 0) {
					for (BannerGroup item : Unsteady.List_BannerGroup) {
						for(Banner bann : item.getCOL_banner()){
							URL url = new URL(bann.getCOL_imgURL());
							
					        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
					        connection.setDoInput(true);
					        connection.connect();
					        InputStream input = connection.getInputStream();
					        Bitmap myBitmap = BitmapFactory.decodeStream(input);

							Unsteady.bannerBitmap.add(myBitmap);
							
						}
					
					}
				}else{
					Log.i("Banner : DownloadImageTask == Null");
				}
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		 
		
		 handler.sendEmptyMessage(0);	
	}
	
	 private Handler handler = new Handler() {																		// HANDLER
		    @Override
		    public void handleMessage(Message msg) {
		    	
		    	
		    	
//		    	insertDecorView(GroupMainActivity.mLocalActivityManager.startActivity("News_Activity",new Intent(Main_theStar10.this,News_Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
//						.getDecorView()); GroupMainActivity.mLocalActivityManager.destroyActivity("", false);
//		    	Intent in = new Intent(Main_theStar10.this, GroupMainActivity.class);
//        		startActivity(in);
//        		finish();
						startActivity(new Intent(Main_theStar10.this,News_Activity.class));
		    }
	 };
	 
	 private void insertDecorView(View view) {
			getParent().setContentView(
					view,
					new LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.FILL_PARENT));
		}
	
}
