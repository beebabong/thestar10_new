package com.gmm.thestar10.ApplicationConfig.login;

public class OneTimePassword {
	private String statusOTP;
	private int statusCode;
	private String statusDetail;
	public String getStatusOTP() {
		return statusOTP;
	}
	public void setStatusOTP(String statusOTP) {
		this.statusOTP = statusOTP;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusDetail() {
		return statusDetail;
	}
	public void setStatusDetail(String statusDetail) {
		this.statusDetail = statusDetail;
	}
	public boolean isCheckStatusOTP(){
		if(statusOTP.trim().equalsIgnoreCase("OK") ){
			return true;
		}
		return false;
	}
	public boolean isCheckStatusCode(){
		if(statusCode == 200 ){
			return true;
		}
		return false;
	}
}
