//package com.gmm.thestar10.ApplicationConfig.login;
//
//import java.util.ArrayList;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.app.AlertDialog.Builder;
//import android.app.Dialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.view.KeyEvent;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout.LayoutParams;
//import android.widget.TextView;
//import android.widget.TextView.OnEditorActionListener;
//
//import com.gmm.thestar10.R;
//import com.gmm.thestar10.ApplicationConfig.GroupMainActivity;
//import com.gmm.thestar10.Untilitys.Constants;
//import com.gmm.thestar10.Untilitys.Log;
//import com.gmm.thestar10.Untilitys.Unsteady;
//import com.gmm.thestar10.object.database.Profile;
//
//public class OTPSecurity {
//	private Activity mActivity;
//	private Class<?> intentClass;
//	private TextView  txtLogin;
//	private String str_msisdn;
//	private String str_onetimepass;
//	public OTPSecurity(Activity mActivity,Class<?> classIntent, TextView  txtLogin) {
//		this.mActivity = mActivity;
//		this.intentClass = classIntent;
//		this.txtLogin =  txtLogin;
//	}
//	public synchronized  void OneTimePasswordLogin(){
//			final Dialog d = new Dialog(mActivity, R.style.CustomDialogTheme);
//			d.setContentView(R.layout.popup_msisdn);
//			d.show();
//
//			final EditText et_msisdn = (EditText) d.findViewById(R.id.popupmsisdn_et_msisdn);
//			et_msisdn.setOnEditorActionListener(new OnEditorActionListener() {
//				// @Override
//				public boolean onEditorAction(TextView v, int actionId,KeyEvent event) {
//
//					// Hide keyBoard
//					InputMethodManager in = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
//					in.hideSoftInputFromWindow(et_msisdn.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
//
//					// Set
//					str_msisdn = null;
//					str_msisdn = et_msisdn.getText().toString().trim();
//					str_msisdn = "66" + str_msisdn.substring(1);
//					return false;
//				}
//			});
//
//			ImageView imgCancel = (ImageView) d.findViewById(R.id.popupmsisdn_butt_cancel);
//			imgCancel.setOnClickListener(new OnClickListener() {
//
//				public void onClick(View v) {
//					d.dismiss();
//				}
//			});
//
//			ImageView imgOk = (ImageView) d.findViewById(R.id.popupmsisdn_butt_ok);
//			imgOk.setOnClickListener(new OnClickListener() {
//
//				public void onClick(View v) {
//
//					Log.i("press : i_msisdn_butt_ok");
//
//					if (!et_msisdn.getText().toString().trim().equals("")) {
//						str_msisdn = et_msisdn.getText().toString().trim();
//						str_msisdn = "66" + str_msisdn.substring(1);
//					}
//
//					if (str_msisdn != null && !str_msisdn.equals("")&& str_msisdn.length() == 11) {
//						
//						Log.i("str_msisdn ::" + str_msisdn);
//						
//						d.dismiss();
//
//						// post Send OTP : status
//						ArrayList<OneTimePassword> statusOTP = null;
//						try {
//							statusOTP = HttpPostRequestOTP.executeHttpPostRequestOTP(Unsteady.Device,str_msisdn);
//							
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//						
//						if (statusOTP != null) { 
//								if (statusOTP.get(0).isCheckStatusCode() && statusOTP.get(0).isCheckStatusOTP()) {
//									// status : OK
//									Unsteady.msisdn = str_msisdn;
//									// next popup enter OTP
//									popupEnterOTP();
//
//								} else {
//
//									// status : ERR
//									// alert
//									String title = "Status send phone number";
//									String message = null;
//									if (statusOTP.get(0).getStatusDetail().trim().equals("")) {
//										message = "not success";
//									} else {
//										message = statusOTP.get(0).getStatusDetail();
//									}
//									showNoticeSendOTPErrDialogBox(title, message);
//								}
//						}else{
//							ServiceApp.showNoticeDialogConnectionInternet(mActivity);
//						}
//
//					} else {
//						// alert : Please, Enter your phone number.
//						String title = mActivity.getString(R.string.popup_msisdn_title);
//						String message = mActivity.getString(R.string.popup_msisdn_msg);
//						showNoticeDialogBox(title, message);
//					}
//				}
//			});
//
//		}
//		
//		
//		public void popupEnterOTP() {
//			final Dialog d = new Dialog(mActivity, R.style.CustomDialogTheme);
//			d.setContentView(R.layout.popup_otp);
//			d.show();
//
//			final EditText et_onetimepass = (EditText) d.findViewById(R.id.popupotp_et_otp);
//			et_onetimepass.setOnEditorActionListener(new OnEditorActionListener() {
//				@Override
//				public boolean onEditorAction(TextView v, int actionId,KeyEvent event) {
//
//					// Hide keyBoard
//					InputMethodManager in = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
//					in.hideSoftInputFromWindow(et_onetimepass.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
//
//					// Set
//					str_onetimepass = null;
//					str_onetimepass = et_onetimepass.getText().toString().trim();
//
//					return false;
//				}
//			});
//
//			ImageView imgCancel = (ImageView) d.findViewById(R.id.popupotp_butt_cancel);
//			imgCancel.setOnClickListener(new OnClickListener() {
//
//				public void onClick(View v) {
//
//					d.dismiss();
//
//					OneTimePasswordLogin();
//				}
//			});
//
//			ImageView imgOk = (ImageView) d.findViewById(R.id.popupotp_butt_ok);
//			imgOk.setOnClickListener(new OnClickListener() {
//
//				public void onClick(View v) {
//						System.out.println("-- OTP");
//						Log.i("press : i_otp_butt_ok");
//						d.dismiss();
//						if (!et_onetimepass.getText().toString().trim().equals("")) {
//							str_onetimepass = et_onetimepass.getText().toString().trim();
//						}
//
//						if (str_onetimepass != null && !str_onetimepass.equals("")) {
//
//							// post Check OTP : status
//							ArrayList<OneTimePassword> statusOTP = new ArrayList<OneTimePassword>();
//							try {
//								statusOTP = HttpPostValidateOTP.executeValidateOTPHttpPost(Unsteady.Device, str_msisdn,str_onetimepass);
//							} catch (Exception e) {
//								e.printStackTrace();
//							}
//							if (statusOTP != null) {
//
//								if (statusOTP.get(0).isCheckStatusCode() && statusOTP.get(0).isCheckStatusOTP()) {
//								Player_Obj.Check_member=true;
//									
//									// status : OK
//									Log.i(""+statusOTP.get(0).getStatusCode()+" : "+statusOTP.get(0).getStatusOTP()+"===================");
//									
//										// check member
//										AccountDetail account = new AccountDetail();
//										try {
//											Log.i("MSISDN : "+Unsteady.msisdn + " : "+Unsteady.Device);
//											account = HttpPostAccountDetail.execute(Unsteady.msisdn);
//											Log.i("--- statusCode Member : "+ account.getStatusCode());
//										} catch (Exception e) {
//											e.printStackTrace();
//										}
//										
//										if (account.isCheckStatusCode()) {
//						//------------------------------------- player obj OTP เเล้วเป็น member
//											if(intentClass == AlbumPageActivity.class){
//												Player_Obj.Check_member=true;
//												Player_Obj.MexSeek = MyPlayer.mDuration;
//												MyPlayer.seekBar.setMax(Player_Obj.MexSeek);
//												AlbumPageActivity.liPopup.setVisibility(View.GONE);
//											}else if(intentClass == VideoPlayerActivity.class){
//												VideoControllerView vdocontroller = new VideoControllerView(mActivity,true);
//												VideoPlayerActivity.player_PopupListen30.setVisibility(View.GONE);
//												VideoPlayerActivity.fullsong =true;
//												Log.i("Player_Obj.MexSeek"+Player_Obj.MexSeek);
////												vdocontroller.mProgress.setMax(Player_Obj.MexSeek);
//												vdocontroller.duration_setprogress =  Player_Obj.MexSeek;
////												vdocontroller.progressBar.setMax(Player_Obj.MexSeek);
//												vdocontroller.duration = VideoControllerView.mPlayer.getDuration();
//												Player_Obj.Check_member = true;
////												vdocontroller.show();
//											}else if(intentClass ==Player_playlistSong.class){
//												Player_Obj.Check_member=true;
//												Player_Obj.MexSeek = MyPlayer.mDuration;
//												MyPlayer.seekBar.setMax(Player_Obj.MexSeek);
//												Player_playlistSong.player_PopupListen30.setVisibility(View.GONE);
//											}else if(intentClass ==Player_playlistVDO.class){
//												Player_Obj.Check_member=true;
//												Player_Obj.MexSeek = MyPlayer.mDuration;
//												MyPlayer.seekBar.setMax(Player_Obj.MexSeek);
//											}else if(intentClass == MainPlaylistActivity.class){
//												Log.i("main");
//												GroupMainActivity.flag_intent = 4;
//												 Intent go = new Intent(mActivity,GroupMainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//												 mActivity.startActivity(go);
////												insertDecorView(GroupMainActivity.mLocalActivityManager.startActivity("MainPlaylistActivity",new Intent(mActivity,MainPlaylistActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
////										  					.getDecorView()); GroupMainActivity.mLocalActivityManager.destroyActivity("", false);
////												
//											}
//									
//											
//											// member
//											// insert db use add to
//												Log.d("profile : "+Unsteady.msisdn+" // "+account.getPacketId()+" // "+account.getDateExpire()+"//"+Unsteady.currentCalendar7Day);
//											Profile myProfile = new Profile();
//
//											myProfile.setMsisdn(Unsteady.msisdn);
//											myProfile.setPackageType(account.getPacketId());
//											myProfile.setExpireddateMember(account.getDateExpire());
//											myProfile.setExpireddateApp(Unsteady.currentCalendar7Day);
//											myProfile.setMemberFlag("Y");
//											
//
//											// ==========================================Manage Dat Application========================================//
//
//											ProfileCriteriaDB.deleteProfile(mActivity, null, null);
//											ProfileCriteriaDB.insertProfile(mActivity, myProfile);
//										
//											// Set Button Login complete
//											if(txtLogin != null){
//											//	txtLogin.setBackgroundResource(R.drawable.btn_logouttelno);
//												txtLogin.setText("            "+ 0+ (Unsteady.msisdn.substring(2, 11)));
//											}
////											else if(intentClass == AlbumPageActivity_schema.class){
////												Intent gotolistview_playlist = new Intent(mActivity,AlbumPageActivity_schema.class);
////												
////												gotolistview_playlist.putExtra("albumId", FirstPage.Album_ID);
////												gotolistview_playlist.putExtra("cpID",  FirstPage.cpID);
////												gotolistview_playlist.putExtra("mediaCode",  FirstPage.mediaCode);
////												gotolistview_playlist.putExtra("gmmdCode",  FirstPage.gmmdCode);
//												
//												/*
//												gotolistview_playlist.putExtra("albumId", "7859");
//												gotolistview_playlist.putExtra("cpID", "1");
//												gotolistview_playlist.putExtra("mediaCode", "0011");
//												gotolistview_playlist.putExtra("gmmdCode", "1204224603");
//												*/
//											//	mActivity.startActivity(gotolistview_playlist);
//												
////											}else if(intentClass == ArtistPageActivity_schema.class){
////												Intent gotolistview_playlist = new Intent(mActivity,SetListViewSong_Schema.class);
////												
////												gotolistview_playlist.putExtra("serviceID", FirstPage.ArcpID);
////												gotolistview_playlist.putExtra("MSISDN",  FirstPage.ArartistID);
////												gotolistview_playlist.putExtra("playListID", FirstPage.ArgmmdCode);
////												
////												mActivity.startActivity(gotolistview_playlist);
//												
////											}else if(intentClass == CommunitySubScribePlayListActivity_.class){
////												
////												Intent in = new Intent(mActivity,CommunitySubScribePlayListActivity_.class);
////												
////												in.putExtra("PlayListID",  FirstPage.playListID);
////												in.putExtra("PlayListName",  FirstPage.playListName);
////												in.putExtra("OwnName", FirstPage.accountdetail.getAccountName());
////												in.putExtra("ServiceID", FirstPage.Service_ID);
////												in.putExtra("FriendMSISDN", FirstPage.MSISDN);
////												if(Unsteady.msisdn.equals( FirstPage.MSISDN)) in.putExtra("value_status", 1);
////												else  in.putExtra("value_status", 0);
////												mActivity.startActivity(in);
////												
////											}
////											else if(intentClass != null){
////												mActivity.startActivity(new Intent(mActivity,intentClass).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
////											}
//											
//										
//
//										} else {
//											// not member
//
//											// insert flag n
//											// String date = (mYear + (mMonth+1) +
//											// mDay)+"";
//
//											Profile profile = new Profile();
//											profile.setMsisdn(Unsteady.msisdn);
//											profile.setPackageType(account.getPacketId());
//											profile.setExpireddateApp(account.getCurrentCalendar7Day());
//											profile.setExpireddateMember(account.getDateExpire());
//											profile.setMemberFlag("N");
//
//											ProfileCriteriaDB.deleteProfile(mActivity, null, null);
//
//											ProfileCriteriaDB.insertProfile(mActivity, profile);
//
//											// Intent Web Register
//											String url = "http://directory.gmmwireless.com/aismusic/register/subscript.jsp?DEVICE="+ Unsteady.Device+ "&APP_ID="+ Constants.APP_ID;
//											Intent in = new Intent(mActivity,WebFullScreenActivity.class);
//											mActivity.startActivity(in);
//										}
//
//									} else {
//
//										// status : ERR
//
//										// alert
//										String title = "Status send OTP";
//										String message;
//										if (statusOTP.get(0).getStatusDetail().trim().equals("")) {
//											message = mActivity.getString(R.string.popup_otp_msg_err);
//										} else {
//											message = statusOTP.get(0).getStatusDetail();
//										}
//										showNoticeValidateErrDialogBox(title, message);
//									}
//							}
//
//						} else {
//
//							// alert
//							String title = mActivity.getString(R.string.popup_otp_title);
//							String message = mActivity.getString(R.string.popup_otp_msg);
//							showNoticeDialogBox(title, message);
//						}
//
//					
//
//				}
//			});
//		}
//		private void showNoticeValidateErrDialogBox(final String title,
//				final String message) {
//
//			Log.i("---- showNoticeDialogBox");
//			Builder setupAlert;
//			setupAlert = new AlertDialog.Builder(mActivity);
//			setupAlert.setTitle(title);
//			setupAlert.setMessage(message);
//			setupAlert.setPositiveButton("Ok",
//					new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int whichButton) {
//							// //popupEnterPhoneNum();
//							popupEnterOTP();
//						}
//					});
//			setupAlert.show();
//		}
//		private void showNoticeDialogBox(final String title, final String message) {
//
//			Log.i("---- showNoticeDialogBox");
//			Builder setupAlert;
//			setupAlert = new AlertDialog.Builder(mActivity);
//			setupAlert.setTitle(title);
//			setupAlert.setMessage(message);
//			setupAlert.setPositiveButton("Ok",
//					new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int whichButton) {
//
//						}
//					});
//			setupAlert.show();
//		}
//		
//		private void showNoticeSendOTPErrDialogBox(final String title,
//				final String message) {
//
//			Log.i("---- showNoticeSendOTPErrDialogBox");
//			Builder setupAlert;
//			setupAlert = new AlertDialog.Builder(mActivity);
//			setupAlert.setTitle(title);
//			setupAlert.setMessage(message);
//			setupAlert.setPositiveButton("Ok",
//					new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int whichButton) {
//							OneTimePasswordLogin();
//						}
//					});
//			setupAlert.show();
//		}
//		
//		private void insertDecorView(View view) {
//			mActivity.setContentView(
//					view,
//					new LayoutParams(LayoutParams.FILL_PARENT,
//							LayoutParams.FILL_PARENT));
//		}
//}
