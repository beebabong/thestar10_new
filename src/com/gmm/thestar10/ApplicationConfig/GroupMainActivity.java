package com.gmm.thestar10.ApplicationConfig;

import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.gmm.thestar10.menu_gallery.Gallery_Activity;
import com.gmm.thestar10.menu_gallery.InGallery_Activity;
import com.gmm.thestar10.menu_news.InNews_Activity;
import com.gmm.thestar10.menu_news.News_Activity;
import com.gmm.thestar10.menu_news.Photo_Of_News_Activity;
import com.gmm.thestar10.menu_setting.Setting_Activity;
import com.gmm.thestar10.menu_video.InVideo_Activity;
import com.gmm.thestar10.menu_video.Video_Activity;

@SuppressWarnings("deprecation")
public class GroupMainActivity extends ActivityGroup{

	public static LocalActivityManager mLocalActivityManager;
	public static int flag_intent=0;
	public static Context c;
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		View view = null ;
		super.onCreate(savedInstanceState);
		
		
		mLocalActivityManager = getLocalActivityManager();
	    if(flag_intent==0){
		 view = mLocalActivityManager 
	                   .startActivity("InNews_Activity", new Intent(this,InNews_Activity.class) 
	                   .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
	                   .getDecorView(); 
	    }else if(flag_intent==1){
			 view = mLocalActivityManager 
                     .startActivity("InVideo_Activity", new Intent(this,InVideo_Activity.class) 
                     .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
                     .getDecorView(); 
	    }else if(flag_intent==2){
			 view = mLocalActivityManager 
                     .startActivity("InGallery_Activity", new Intent(this,InGallery_Activity.class) 
                     .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
                     .getDecorView(); 
	    }else if(flag_intent==3){
			 view = mLocalActivityManager 
                     .startActivity("Setting_Activity", new Intent(this,Setting_Activity.class) 
                     .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
                     .getDecorView(); 
	    }
	    
	      
	    this.setContentView(view);
	}
	
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		View view = null ;
		String current_page = mLocalActivityManager.getCurrentId();
		
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			
			if(current_page.equals("InNews_Activity")){
//				view = mLocalActivityManager 
//	                    .startActivity("News_Activity", new Intent(this,News_Activity.class) 
//	                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
//	                    .getDecorView(); 
				startActivity( new Intent(this,News_Activity.class)) ;
			}
			else if(current_page.equals("InVideo_Activity")){
//				view = mLocalActivityManager 
//	                    .startActivity("Video_Activity", new Intent(this,Video_Activity.class) 
//	                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
//	                    .getDecorView(); 

				startActivity( new Intent(this,Video_Activity.class)) ;
			}
			else if(current_page.equals("InGallery_Activity")){
//				view = mLocalActivityManager 
//	                    .startActivity("Gallery_Activity", new Intent(this,Gallery_Activity.class) 
//	                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
//	                    .getDecorView(); 

				startActivity( new Intent(this,Gallery_Activity.class)) ;
			}
			else if(current_page.equals("Photo_Of_News_Activity")){
				view = mLocalActivityManager 
	                    .startActivity("InNews_Activity", new Intent(this,InNews_Activity.class) 
	                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
	                    .getDecorView(); 
				 this.setContentView(view);
			}
			else if(current_page.equals("Show_Gallery_Activity_news")){
				view = mLocalActivityManager 
	                    .startActivity("Photo_Of_News_Activity", new Intent(this,Photo_Of_News_Activity.class) 
	                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
	                    .getDecorView(); 
				 this.setContentView(view);
			}
			else if(current_page.equals("Show_Gallery_Activity")){
				view = mLocalActivityManager 
	                    .startActivity("InGallery_Activity", new Intent(this,InGallery_Activity.class) 
	                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
	                    .getDecorView(); 
				 this.setContentView(view);
			}
			
		}
		
		return false;
	}
	
public void exit(){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Exit ?");
	
		builder.setMessage("คุณต้องการออกจากแอพพลิเคชั่น ")
		       .setCancelable(true)
		       
		       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
						Intent startMain = new Intent(
								Intent.ACTION_MAIN);
						startMain.addCategory(Intent.CATEGORY_HOME);
						startMain
								.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(startMain);
						onDestroy();
						android.os.Process
								.killProcess(android.os.Process.myPid());
		           }
		       })
		       .setNegativeButton("No", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                dialog.cancel();
		           }
		       });
		 AlertDialog alert = builder.create();
		 alert.show();
	}

}
