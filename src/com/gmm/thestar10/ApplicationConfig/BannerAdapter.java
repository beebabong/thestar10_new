package com.gmm.thestar10.ApplicationConfig;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.gmm.thestar10.R;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.Untilitys.Unsteady;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class BannerAdapter implements OnClickListener{
	private Context mContext;
	private ViewFlipper flipper;
	private ImageView img_prev_banner,img_next_banner,imgBanner;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	int positionbanner;
	
	
	public BannerAdapter(Context cntext, ViewFlipper viewFlipper) {
		this.flipper = viewFlipper;
		this.mContext = cntext;
		init();
	}

	@SuppressWarnings("deprecation")
	public void init() {
//		flipper.setInAnimation(AnimationUtils.loadAnimation(mContext,R.anim.left_in));
//		flipper.setOutAnimation(AnimationUtils.loadAnimation(mContext,R.anim.left_out));	
		
		
		if ((Unsteady.bannerBitmap != null && Unsteady.bannerBitmap.size() > 0 )) {
			flipper.removeAllViews();	
			for (Bitmap  item : Unsteady.bannerBitmap) {
				LayoutInflater inflater = LayoutInflater.from(mContext);
				View row = null;
				row = inflater.inflate(R.layout.item_banner,null);
				imgBanner = (ImageView)row.findViewById(R.id.img_banner);
				img_prev_banner = (ImageView)row.findViewById(R.id.img_prev_banner);
				img_next_banner = (ImageView)row.findViewById(R.id.img_next_banner);
				
				img_prev_banner.setOnClickListener(this);
				img_next_banner.setOnClickListener(this);
				
				imgBanner.setImageBitmap(item);
				flipper.addView(row);
			}		
			flipper.startFlipping();
			flipper.setFlipInterval(5000);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_prev_banner:
			flipper.showPrevious();
			break;
			
		case R.id.img_next_banner:
			flipper.showNext();
			Log.i("next");
			break;

		
		}
	}


	
}
