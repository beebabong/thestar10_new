package com.gmm.thestar10.ApplicationConfig;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;

import com.gmm.thestar10.Untilitys.Constants;
import com.gmm.thestar10.Untilitys.Log;

public final class DeviceInfo {
	// get Devince ID
	public static final void getDeviceID(Context mContext) {
		Log.i("DeviceInfo =|= getDeviceID");
		try {
			TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
			Constants.DEVICE = telephonyManager.getDeviceId();
			Log.i(" DeviceID : " + Constants.DEVICE);
		} catch (Exception e) {
		}
		try {
			if (Constants.DEVICE == null || Constants.DEVICE.equals("")) {
				WifiManager wifiMan = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
				WifiInfo wifiInf = wifiMan.getConnectionInfo();
				Constants.DEVICE = wifiInf.getMacAddress();
				Log.i(" DeviceID : " + Constants.DEVICE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// check internet
	public static boolean isCheckInternet(Context con) {
		ConnectivityManager connec = (ConnectivityManager) con.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
			// connect
			return true;
		} else {
			// not-connect
			return false;
		}
	}
	
	public static String getConnectionType( Context context) {
		Log.i("--getConnectionType");

		String connectionType = "";

		ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		State mobile = conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.getState();
		State wifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
				.getState();

		if (mobile == NetworkInfo.State.CONNECTED
				|| mobile == NetworkInfo.State.CONNECTING) {
			connectionType = "3G";
		}

		if (wifi == NetworkInfo.State.CONNECTED
				|| wifi == NetworkInfo.State.CONNECTING) {
			connectionType = "WIFI";
		}

		Log.i("== connection : " + connectionType);
		return connectionType;
	}
}
