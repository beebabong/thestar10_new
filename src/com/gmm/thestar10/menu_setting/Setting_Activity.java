package com.gmm.thestar10.menu_setting;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.gmm.thestar10.Prefs;
import com.gmm.thestar10.R;
import com.gmm.thestar10.menu_Adapter;
import com.gmm.thestar10.API.RegTokenAPI;
import com.gmm.thestar10.ApplicationConfig.GroupMainActivity;
import com.gmm.thestar10.Untilitys.Constants;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.facebook.android.FacebookHelper;
import com.gmm.thestar10.facebook.android.SessionEvents;
import com.gmm.thestar10.facebook.android.SessionEvents.AuthListener;
import com.gmm.thestar10.facebook.android.SessionEvents.LogoutListener;
import com.gmm.thestar10.facebook.android.SessionStore;
import com.gmm.thestar10.flyoutmenu.FlyOutContainer;
import com.gmm.thestar10.menu_downloaded_song_mv.MainDownloaded;
import com.gmm.thestar10.menu_gallery.Gallery_Activity;
import com.gmm.thestar10.menu_news.News_Activity;
import com.gmm.thestar10.menu_video.Video_Activity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class Setting_Activity extends Activity implements OnClickListener,OnTouchListener{
	// menu
			FlyOutContainer root;
			ListView listview_main_menu;
			ImageView btn_menu;
	
	//Activity in this
			ImageView btn_howtovote,img_profile,img_login_fb,icon_howto,img_noti;
			TextView txt_name_fb,txt_login;
			LinearLayout layout_main;
			
	//Other
		    DisplayImageOptions options;
			protected ImageLoader imageLoader = ImageLoader.getInstance();
			private static final int AUTHORIZE_ACTIVITY_RESULT_CODE = 0;
			
			public SessionListener mSessionListener = new SessionListener();

			private Handler mHandler = new Handler();
			private String[] permissions = { "offline_access", "publish_stream","email",
					"user_photos", "publish_checkins", "photo_upload" };

			private ProgressDialog pd;
			private String mStatusToken;
			float downX = 0, downY = 0, upX, upY;
			boolean check_noti = false;
			@Override
			protected void onCreate(Bundle savedInstanceState) {
				super.onCreate(savedInstanceState);
				this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.ac_setting, null);
				this.setContentView(root);
				
				 options = new DisplayImageOptions.Builder()
					.showStubImage(R.drawable.img_profile)
					.showImageForEmptyUri(R.drawable.img_profile)
					.showImageOnFail(R.drawable.img_profile).cacheInMemory()
					.cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).build();
			     imageLoader.init(ImageLoaderConfiguration.createDefault(Setting_Activity.this));

			        
				setmenu();
				mapping();
				setAction();
				
				FacebookHelper.mFacebook = new Facebook(Constants.APP_FACEBOOK_ID);
				SessionEvents.addAuthListener(mSessionListener);
			}
	
	
			@Override
			protected void onResume() {
				// TODO Auto-generated method stub
				super.onResume();
				if (FacebookHelper.mFacebook != null) {
					mHandler = new Handler();
					if (SessionStore.restore(FacebookHelper.mFacebook,
							Setting_Activity.this)|| FacebookHelper.mFacebook.isSessionValid()) {
						//Log.i("-- login Complete");
						SharedPreferences prefs = Prefs.get(Setting_Activity.this);
						String[] session = prefs.getString("facebookSession", "").split(",");
						
						if (session[1].length() > 23) {
							txt_name_fb.setText(session[1].substring(0, 22)+ "..");
						} else {
							txt_name_fb.setText(session[1]);
						}
						imageLoader.displayImage(session[2], img_profile, options);
					} else {
						img_profile.setImageResource(R.drawable.img_profile);
					}
				}
				
				loadSavedPreferences();

			}
			
			private void loadSavedPreferences() {
				SharedPreferences sharedPreferences = PreferenceManager
						.getDefaultSharedPreferences(this);
				check_noti = sharedPreferences.getBoolean("notification", false);
				if (check_noti) {
					img_noti.setImageResource(R.drawable.btn_on);
				} else {
					img_noti.setImageResource(R.drawable.btn_off);
				}
			}

	private void setAction() {
		layout_main.setOnTouchListener(this);
		img_login_fb.setOnClickListener(this);
		img_noti.setOnClickListener(this);
	}
	
	private void mapping() {
		btn_howtovote = (ImageView) root.findViewById(R.id.btn_howtovote);
		img_profile = (ImageView) root.findViewById(R.id.img_profile);
		img_login_fb = (ImageView) root.findViewById(R.id.img_login_fb);
		icon_howto = (ImageView) root.findViewById(R.id.icon_howto);
		img_noti = (ImageView) root.findViewById(R.id.img_noti);
		txt_name_fb = (TextView) root.findViewById(R.id.txt_name_fb);
		txt_login = (TextView) root.findViewById(R.id.txt_login);
		layout_main = (LinearLayout) root.findViewById(R.id.layout_main);
	}
	
	//******************************************************************************************************************************************************
		//********************************************************************SET MENU**************************************************************************
		//******************************************************************************************************************************************************
		
		private void setmenu() {
			listview_main_menu = (ListView) root.findViewById(R.id.listview_main_menu);
			btn_menu = (ImageView) root.findViewById(R.id.btn_menu);
			
			listview_main_menu.setAdapter(new menu_Adapter(this,3));

			btn_menu.setOnClickListener(this);
			listview_main_menu.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View v, int pos,
						long arg3) {
					Log.i("click :"+pos);
					switch (pos) {

					case 0: startActivity(new Intent(Setting_Activity.this,News_Activity.class)); break;//ActivityTarget="MainDownloaded"; classTarget = MainDownloaded.class; break;
			        case 1: startActivity(new Intent(Setting_Activity.this,Video_Activity.class)); break;//ActivityTarget="Setting_Activity"; classTarget = Setting_Activity.class; 
					case 2: startActivity(new Intent(Setting_Activity.this,Gallery_Activity.class)); break;//ActivityTarget="Setting_Activity"; classTarget = Setting_Activity.class; break;
//			        case 3: startActivity(new Intent(Setting_Activity.this,Setting_Activity.class)); break;//ActivityTarget="Setting_Activity"; classTarget = Setting_Activity.class; break;
			        case 4: startActivity(new Intent(Setting_Activity.this,MainDownloaded.class)); break;//ActivityTarget="MainDownloaded"; classTarget = MainDownloaded.class; break;
			

					}    
				}
			});
		}

		private void insertDecorView(View view) {
			getParent().setContentView(
					view,
					new LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.FILL_PARENT));
		}
	
	
		//******************************************************************************************************************************************************
		//************************************************************ SET CLICK / TOUCH ***********************************************************************
		//******************************************************************************************************************************************************

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_menu:
				root. toggleMenu();
				break;
			case R.id.btn_howtovote:
				break;
			case R.id.img_profile:
				break;
			case R.id.img_login_fb:
				if (FacebookHelper.mFacebook != null) {
					System.out.println("not null--");
					if (SessionStore.restore(FacebookHelper.mFacebook,Setting_Activity.this) || FacebookHelper.mFacebook.isSessionValid()) {
						System.out.println("kick if");
						//Log.i("-- Logout facebook");
						 pd = ProgressDialog.show(Setting_Activity.this, "Loading..","Please wait . .", true, false);
						SessionEvents.onLogoutBegin();
						AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(FacebookHelper.mFacebook);

						// clear session
						SessionStore.clear(Setting_Activity.this);
						asyncRunner.logout(Setting_Activity.this, new RequestListener() {
									public void onComplete(String response,Object state) {
										mHandler.post(new Runnable() {
											@Override
											public void run() {
												img_profile.setImageResource(R.drawable.img_profile);
												txt_name_fb.setText("");
												SharedPreferences sharedPrefs = Prefs.get(Setting_Activity.this);
												sharedPrefs.edit().remove("facebookSession").commit();
												
												pd.dismiss();
											}
										});
									}
									public void onIOException(IOException e,Object state) {
										//Log.e("-------- logout facebook onIOEexception");
									}

									public void onFileNotFoundException(FileNotFoundException e,Object state) {
										//Log.e("-------- logout facebook onFileNotFoundException");
									}

									public void onMalformedURLException(MalformedURLException e,Object state) {
										//Log.e("-------- logout facebook onMalformedURLException");
									}

									public void onFacebookError(FacebookError e, Object state) {
										//Log.e("-------- logout facebook onFacebookError");
									}
								});
					
					} else {
						FacebookHelper.mFacebook.authorize(
								Setting_Activity.this, permissions,AUTHORIZE_ACTIVITY_RESULT_CODE,new LoginDialogListener());
					}
				}else {
					System.out.println(" null--");
				}
			
				break;
			case R.id.txt_login:
				break;
			case R.id.icon_howto:
				break;
			case R.id.img_noti:
				if(check_noti) {
					savePreferences("notification",false );
					RegTokenAPI.getJSONFromUrl("N");
				}
				else {
					savePreferences("notification",true );
					RegTokenAPI.getJSONFromUrl("Y");
				}
				
				loadSavedPreferences();
				break;
			}
			
		}

		private void savePreferences(String key, boolean value) {
			SharedPreferences sharedPreferences = PreferenceManager
					.getDefaultSharedPreferences(this);
			Editor editor = sharedPreferences.edit();
			editor.putBoolean(key, value);
			editor.commit();
		}
		
		
		
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {

			int MIN_DISTANCE = 100;
			if(downX==0) downX = event.getX();
			if(downY==0) downY = event.getY();
			
			 switch(event.getAction()){
			 
		        case MotionEvent.ACTION_UP: {
		            upX = event.getX();
		            upY = event.getY();

		            float deltaX = downX - upX;

		            if(Math.abs(deltaX) > MIN_DISTANCE){
		                // left or right
		                if(deltaX < 0 || deltaX > 0) { root. toggleMenu(); }
		            }
		           
		            return true;
		        }
		    }
		    return false;
		}
		
		
		public class SessionListener implements AuthListener, LogoutListener {

			@Override
			public void onAuthSucceed() {
				SessionStore.save(FacebookHelper.mFacebook, Setting_Activity.this);
				try {
					//SessionStore.restore(mFacebook, SettingsActivity.this);
					
					new MyAsyncTask().execute("");
					
				} catch (Exception e) {
					Toast.makeText(Setting_Activity.this,"Exception" + e.toString(), Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void onAuthFail(String error) {
			}

			@Override
			public void onLogoutBegin() {
				//Log.i("Start Logout Facebook");
				pd = ProgressDialog.show(Setting_Activity.this, "Loading..",
						"Please waite . .", true, false);
			}

			@Override
			public void onLogoutFinish() {
				//Log.i("Logout Facebook");
				SessionStore.clear(Setting_Activity.this);
//				imgLoginFB.setBackgroundResource(R.drawable.btn_connectfacebook);
//				setting_name_facebook.setVisibility(View.GONE);
				pd.dismiss();
			}
		}

		
		class MyAsyncTask extends AsyncTask<String,Void,Boolean>
		{
			String name,tokenID,id,ProfilePhotoUrl,session ;
		public Boolean doInBackground(String ...message){
			try {

//		    	Log.i("json facebook :"+FacebookHelper.mFacebook.request("me"));
					JSONObject json = Util.parseJson(FacebookHelper.mFacebook.request("me"));
					id = json.getString("id");
					tokenID = FacebookHelper.mFacebook.getAccessToken();
					name = json.getString("name");
					ProfilePhotoUrl = "http://graph.facebook.com/"+id+"/picture?type=small";
					
					
//					setting_name_facebook.setVisibility(View.VISIBLE);
					
					session = tokenID+","+name+","+ProfilePhotoUrl+","+id; 
					SharedPreferences prefs = Prefs.get(Setting_Activity.this);
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString("facebookSession", session);
					editor.commit();
					 return Boolean.TRUE;
				} catch (Exception e) {
					Toast.makeText(Setting_Activity.this,"Exception FB " + e.toString(),Toast.LENGTH_SHORT).show();

					 return Boolean.FALSE;
				}
		   
		} 

		public void onPostExecute(Boolean result){
		if(result == Boolean.TRUE){
			if (name.length() > 23) {
				txt_name_fb.setText(name.substring(0, 22)+ "..");
			} else {
				txt_name_fb.setText(name);
			}
			imageLoader.displayImage(ProfilePhotoUrl, img_profile, options);
		
		}
		}
		}
		private final class LoginDialogListener implements DialogListener {
			@Override
			public void onComplete(Bundle values) {
				SessionEvents.onLoginSuccess();
			}

			@Override
			public void onFacebookError(FacebookError error) {
				SessionEvents.onLoginError(error.getMessage());
			}

			@Override
			public void onError(DialogError error) {
				SessionEvents.onLoginError(error.getMessage());
			}

			@Override
			public void onCancel() {
				SessionEvents.onLoginError("Action Canceled");
			}
		}
		//******************************************************************************************************************************************************
		//******************************************************************* EXIT *****************************************************************************
		//******************************************************************************************************************************************************

		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			// TODO Auto-generated method stub
			if (keyCode == KeyEvent.KEYCODE_BACK ){
				exit();
			}
			
			return super.onKeyDown(keyCode, event);
		}
		
		public void exit(){
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Exit ?");
		
			builder.setMessage("คุณต้องการออกจากแอพพลิเคชั่น ")
			       .setCancelable(true)
			       
			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
							Intent startMain = new Intent(
									Intent.ACTION_MAIN);
							startMain.addCategory(Intent.CATEGORY_HOME);
							startMain
									.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(startMain);
							onDestroy();
							android.os.Process
									.killProcess(android.os.Process.myPid());
			           }
			       })
			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			                dialog.cancel();
			           }
			       });
			 builder.show();
		}
		
}
