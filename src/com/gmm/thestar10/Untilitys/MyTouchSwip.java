package com.gmm.thestar10.Untilitys;

import com.gmm.thestar10.flyoutmenu.FlyOutContainer;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class MyTouchSwip implements OnTouchListener {
	FlyOutContainer root;
	int MIN_DISTANCE_OPEN = 300;
	int MIN_DISTANCE_CLOSE = 100;
	float downX = 0, downY = 0, upX=0,upY=0;
	  
	@Override
	   public boolean onTouch(View v, MotionEvent event) {

		 switch(event.getAction() ){
	        
	        case MotionEvent.ACTION_UP: {
	            upX = event.getX();
	            upY = event.getY();

	            float deltaX = downX - upX;
	            float deltaY = downY - upY;

	            Log.i("touch up: "+downX+"//"+upX +"//"+upY+"//"+Math.abs(deltaX)+"//"+Math.abs(deltaY));
	            
	            if (Math.abs(deltaY) > 50)
                   return false;
	            	
	            	if(Math.abs(deltaX) > MIN_DISTANCE_OPEN && downX<upX){
		                // left to right
		                if(deltaX < 0 || deltaX > 0) { root. toggleMenu(); }
	            	}
	            	if(Math.abs(deltaX) > MIN_DISTANCE_CLOSE && downX>upX){
		                // right to left 
		                if(deltaX < 0 || deltaX > 0) { root. toggleMenu(); }
		            }
	            
	            return true;
	        }
	        case MotionEvent.ACTION_DOWN: {
	        	 downX = event.getX();
	        	 downY = event.getY();

	            return true;
	        }
	    }
		 
		 return false;
	}
}
