package com.gmm.thestar10.menu_gallery;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.gmm.thestar10.R;
import com.gmm.thestar10.ApplicationConfig.GroupMainActivity;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.Untilitys.Unsteady;
import com.gmm.thestar10.menu_news.Show_Gallery_Activity;
import com.gmm.thestar10.object.Gallery_Detail;
import com.gmm.thestar10.object.NewsGallery;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class InGallery_Activity extends Activity {
	
	TextView txt_title;
	GridView grid_photogal;
	
	//Other
	 DisplayImageOptions options;
	 protected ImageLoader imageLoader = ImageLoader.getInstance();
	 public static int position;
	 int width=0;
	 List<String> url_Gal = new ArrayList<String>();
	
		@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ac_photo_of_news);
		
		 options = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.img_profile)
			.showImageForEmptyUri(R.drawable.img_profile)
			.showImageOnFail(R.drawable.img_profile).cacheInMemory()
			.cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).build();
	        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
	        
		mapping();
		setAction();
		
	}
	
		@Override
	protected void onResume() {
			super.onResume();
			
		}
		
	private void mapping() {
		txt_title = (TextView) findViewById(R.id.txt_title);
		grid_photogal = (GridView) findViewById(R.id.grid_photoNews);
		
	}

	private void setAction() {
		width = getWindowManager().getDefaultDisplay().getWidth();
		
		txt_title.setText(Unsteady.gal.get(position).getCOL_headline());
		grid_photogal.setAdapter(new ImageAdapter(this));
		
		for(Gallery_Detail gd : Unsteady.gal.get(position).getCOL_galleryDetails()){
			url_Gal.add(gd.getCOL_coverImgDetail());	
		}	
		
		grid_photogal.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				
				Show_Gallery_Activity.url_Gal = url_Gal;
				Show_Gallery_Activity.position = pos;
				
				insertDecorView(GroupMainActivity.mLocalActivityManager.startActivity("Show_Gallery_Activity",new Intent(InGallery_Activity.this,Show_Gallery_Activity.class).putExtra("coneform", "gallery")
				.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
				.getDecorView()); GroupMainActivity.mLocalActivityManager.destroyActivity("", false);
			}
		});
	}
	
	private void insertDecorView(View view) {
		getParent().setContentView(
				view,
				new LayoutParams(LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT));
	}
	

	
	class ImageAdapter extends BaseAdapter {
        private Context mcontext;
        public ImageAdapter(Context c)
        {
        	mcontext=c;
        }

        @Override
        public int getCount() {
        	return Unsteady.gal.get(position).getCOL_galleryDetails().size();
        }

        @Override
        public Object getItem(int position) {
        	return Unsteady.gal.get(position).getCOL_galleryDetails().get(position).getCOL_coverImgDetail();
        }

        @Override
        public long getItemId(int position) {
        	return 0;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
        	ImageView imageView = new ImageView(mcontext);
          imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
          GridView.LayoutParams flpgal = new GridView.LayoutParams(width/2,width/2);
          imageView.setLayoutParams(flpgal);
                        
        	Log.i("link gal : "+ Unsteady.gal.get(position).getCOL_galleryDetails().get(pos).getCOL_coverImgDetail());
        	imageLoader.displayImage(Unsteady.gal.get(position).getCOL_galleryDetails().get(pos).getCOL_coverImgDetail(), imageView, options);
            return imageView;
        }



}
}
