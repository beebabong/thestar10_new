package com.gmm.thestar10.menu_video;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.gmm.thestar10.R;
import com.gmm.thestar10.ApplicationConfig.GroupMainActivity;
import com.gmm.thestar10.Untilitys.Unsteady;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class InVideo_Activity extends Activity implements OnClickListener{
	
	//Activity in this
		ImageView img_screenshot_main,img_screenshot_1,img_screenshot_2,img_next,img_prev;
		TextView txt_headline,txt_datepost,txt_detail;
		LinearLayout subVDO;
		
	//Other
		 DisplayImageOptions options;
		 protected ImageLoader imageLoader = ImageLoader.getInstance();
		 public static int position;
		 int index_vdo1=1,index_vdo2=2,width;
		 Typeface type;
			
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ac_invideo);
		
		 options = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.img_profile)
			.showImageForEmptyUri(R.drawable.img_profile)
			.showImageOnFail(R.drawable.img_profile).cacheInMemory()
			.cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).build();
	        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
	        type=Typeface.createFromAsset(getAssets(),"THSarabun_Bold.ttf");
	        
		mapping();
		setAction();
	}

	private void setAction() {
		width = getWindowManager().getDefaultDisplay().getWidth();
		
		
		LinearLayout.LayoutParams flp= new LinearLayout.LayoutParams(width, 400);
		flp.setMargins(2, 2, 2, 2);
		img_screenshot_main.setLayoutParams(flp);
		if(Unsteady.vdo.get(position).getCOL_videoParts().size()>0)
		imageLoader.displayImage(Unsteady.vdo.get(position).getCOL_videoParts().get(0).getCOL_thumbsImgPart(), img_screenshot_main, options);

		if(Unsteady.vdo.get(position).getCOL_videoParts().size()>2){
			LinearLayout.LayoutParams flp1= new LinearLayout.LayoutParams(width/3, 200);
			flp1.setMargins(2, 2, 2, 2);
			img_screenshot_1.setLayoutParams(flp1);
			if(Unsteady.vdo.get(position).getCOL_videoParts().size()>index_vdo1)
				imageLoader.displayImage(Unsteady.vdo.get(position).getCOL_videoParts().get(index_vdo1).getCOL_thumbsImgPart(), img_screenshot_1, options);
			else img_screenshot_1.setVisibility(View.INVISIBLE);
			
			LinearLayout.LayoutParams flp2= new LinearLayout.LayoutParams(width/3, 200);
			flp2.setMargins(2, 2, 2, 2);
			img_screenshot_2.setLayoutParams(flp2);
			if(Unsteady.vdo.get(position).getCOL_videoParts().size()>index_vdo2)
				imageLoader.displayImage(Unsteady.vdo.get(position).getCOL_videoParts().get(index_vdo2).getCOL_thumbsImgPart(), img_screenshot_2, options);
			else img_screenshot_2.setVisibility(View.INVISIBLE);
		}else{
			subVDO.setVisibility(View.GONE);
		}
		
		
		
		txt_headline.setText(Unsteady.vdo.get(position).getCOL_headline());
		txt_datepost.setText("Posted "+Unsteady.vdo.get(position).getCOL_releaseDate());
		txt_detail.setText(Html.fromHtml(Unsteady.vdo.get(position).getCOL_desc()));
		img_next.setOnClickListener(this);
		img_prev.setOnClickListener(this);
		
		img_screenshot_main.setOnClickListener(this);
		img_screenshot_1.setOnClickListener(this);
		img_screenshot_2.setOnClickListener(this);
	}

	private void mapping() {
		
		
		img_screenshot_main = (ImageView) findViewById(R.id.img_screenshot_main);
		img_screenshot_1 = (ImageView) findViewById(R.id.img_screenshot_1);
		img_screenshot_2 = (ImageView) findViewById(R.id.img_screenshot_2);
		img_next= (ImageView) findViewById(R.id.img_next);
		img_prev= (ImageView) findViewById(R.id.img_prev);
		subVDO = (LinearLayout) findViewById(R.id.subVDO);
		txt_headline= (TextView) findViewById(R.id.txt_headline);
		txt_datepost= (TextView) findViewById(R.id.txt_datepost);
		txt_detail= (TextView) findViewById(R.id.txt_detail);
		

		txt_headline.setTypeface(type);
		txt_datepost.setTypeface(type);
		txt_detail.setTypeface(type);
	}

	
	
	private void insertDecorView(View view) {
		getParent().setContentView(
				view,
				new LayoutParams(LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_prev:
			index_vdo1--;
			index_vdo2--;
			if(index_vdo1>0){
				
				LinearLayout.LayoutParams flp1= new LinearLayout.LayoutParams(width/3, 200);
				flp1.setMargins(2, 2, 2, 2);
				img_screenshot_1.setLayoutParams(flp1);
				if(Unsteady.vdo.get(position).getCOL_videoParts().size()>index_vdo1)
					imageLoader.displayImage(Unsteady.vdo.get(position).getCOL_videoParts().get(index_vdo1).getCOL_thumbsImgPart(), img_screenshot_1, options);
				else img_screenshot_1.setVisibility(View.INVISIBLE);
				
				
				LinearLayout.LayoutParams flp2= new LinearLayout.LayoutParams(width/3, 200);
				flp2.setMargins(2, 2, 2, 2);
				img_screenshot_2.setLayoutParams(flp2);
				if(Unsteady.vdo.get(position).getCOL_videoParts().size()>index_vdo2)
					imageLoader.displayImage(Unsteady.vdo.get(position).getCOL_videoParts().get(index_vdo2).getCOL_thumbsImgPart(), img_screenshot_2, options);
				else img_screenshot_2.setVisibility(View.INVISIBLE);
				break;
			}
			
		case R.id.img_next:
			index_vdo1++;
			index_vdo2++;
			if(index_vdo2<Unsteady.vdo.get(position).getCOL_videoParts().size()){
				
				
				LinearLayout.LayoutParams flp1= new LinearLayout.LayoutParams(width/3, 200);
				flp1.setMargins(2, 2, 2, 2);
				img_screenshot_1.setLayoutParams(flp1);
				if(Unsteady.vdo.get(position).getCOL_videoParts().size()>index_vdo1)
					imageLoader.displayImage(Unsteady.vdo.get(position).getCOL_videoParts().get(index_vdo1).getCOL_thumbsImgPart(), img_screenshot_1, options);
				else img_screenshot_1.setVisibility(View.INVISIBLE);
				
				
				
				LinearLayout.LayoutParams flp2= new LinearLayout.LayoutParams(width/3, 200);
				flp2.setMargins(2, 2, 2, 2);
				img_screenshot_2.setLayoutParams(flp2);
				if(Unsteady.vdo.get(position).getCOL_videoParts().size()>index_vdo2)
					imageLoader.displayImage(Unsteady.vdo.get(position).getCOL_videoParts().get(index_vdo2).getCOL_thumbsImgPart(), img_screenshot_2, options);
				else img_screenshot_2.setVisibility(View.INVISIBLE);
				break;
			}
			
			
		default:
			if(Unsteady.vdo.get(position).getCOL_videoParts().size()!=0){
//				insertDecorView(GroupMainActivity.mLocalActivityManager.startActivity("Show_VDO_Fullscreen",new Intent(InVideo_Activity.this,Show_VDO_Fullscreen.class)
//				.putExtra("VCodePart", Unsteady.vdo.get(position).getCOL_videoParts().get(0).getCOL_googleVCodePart())
//				.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
//				.getDecorView()); GroupMainActivity.mLocalActivityManager.destroyActivity("", false);
				
				startActivity(new Intent(InVideo_Activity.this,Show_VDO_Fullscreen.class));
			}
			
			
			
//			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v="+Unsteady.vdo.get(position).getCOL_videoParts().get(0).getCOL_googleVCodePart())));
			
			
			break;
		}
		
	}
	
	
}
