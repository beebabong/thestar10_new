package com.gmm.thestar10.menu_video;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.gmm.thestar10.R;
import com.gmm.thestar10.menu_Adapter;
import com.gmm.thestar10.API.BannerAPI;
import com.gmm.thestar10.API.VideoAPI;
import com.gmm.thestar10.ApplicationConfig.BannerAdapter;
import com.gmm.thestar10.ApplicationConfig.GroupMainActivity;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.Untilitys.Unsteady;
import com.gmm.thestar10.flyoutmenu.FlyOutContainer;
import com.gmm.thestar10.menu_downloaded_song_mv.MainDownloaded;
import com.gmm.thestar10.menu_gallery.Gallery_Activity;
import com.gmm.thestar10.menu_news.InNews_Activity;
import com.gmm.thestar10.menu_news.News_Activity;
import com.gmm.thestar10.menu_setting.Setting_Activity;
import com.gmm.thestar10.object.Banner;
import com.gmm.thestar10.object.BannerGroup;
import com.gmm.thestar10.object.VideoHightlight;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class Video_Activity extends Activity implements OnClickListener,OnTouchListener,Runnable {
	
	// menu
		FlyOutContainer root;
		ListView listview_main_menu;
		ImageView btn_menu;
		
	//Activity in this
		LinearLayout layout_shownews;
		ViewFlipper viewflipBanner;
		TextView txt_title,txt_subtitle;
		ScrollView scrollview_layout;
		
		
	//Other
	    DisplayImageOptions options;
		protected ImageLoader imageLoader = ImageLoader.getInstance();
		Typeface type;
		float downX = 0, downY = 0, upX, upY;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.ac_news, null);


		 options = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.img_profile)
			.showImageForEmptyUri(R.drawable.img_profile)
			.showImageOnFail(R.drawable.img_profile).cacheInMemory()
			.cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).build();
	        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
	        type=Typeface.createFromAsset(getAssets(),"THSarabun_Bold.ttf");
	        
		setmenu();
		mapping();
		setAction();
		
		this.setContentView(root);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		new Thread(this).start();
		super.onResume();
	}
	
	private void setAction() {
		
		txt_title.setText("Video Hightlight");
		txt_subtitle.setText("Weekly Video Hightlight");
//		scrollview_layout.setOnTouchListener(this);
	}
	
	private void mapping() {
		layout_shownews = (LinearLayout) root.findViewById(R.id.layout_shownews);
		viewflipBanner = (ViewFlipper) root.findViewById(R.id.viewflipBanner);
		txt_title= (TextView) root.findViewById(R.id.txt_title);
		txt_subtitle= (TextView) root.findViewById(R.id.txt_subtitle);
		scrollview_layout = (ScrollView) root.findViewById(R.id.scrollview_layout);
		
	}


	//******************************************************************************************************************************************************
	//****************************************************************SET RUNNABLE**************************************************************************
	//******************************************************************************************************************************************************
	
	@Override
	public void run() {																								// RUN
		
		if(Unsteady.List_BannerGroup.size()==0) Unsteady.List_BannerGroup = new BannerAPI().getJSONFromUrl();
		if(Unsteady.vdo.size()==0) Unsteady.vdo = new VideoAPI().getJSONFromUrl();
		
		handler.sendEmptyMessage(0);	
	}
	
	 private Handler handler = new Handler() {																		// HANDLER
		    @Override
		    
		    public void handleMessage(Message msg) {

		    	new DownloadImageTask().execute();
		    	
		    	for(int i = 0; i < Unsteady.vdo.size(); i++){
    				
    				LayoutInflater inflater = getLayoutInflater();
    				
    				View row = inflater.inflate(R.layout.list_news, null);
    				WrapperShowList wrapper = new WrapperShowList(row);								
    				
    				wrapper.init(Unsteady.vdo.get(i), i);
    				layout_shownews.addView(row);
		    	}
		    }
	 };
	
	 
	//******************************************************************************************************************************************************
	//************************************************************ WrapperShowList *************************************************************************
	//******************************************************************************************************************************************************
		
	 class WrapperShowList{
			private View row = null;
			private TextView txtHeadline = null;
			private TextView txtdate = null;
			private ImageView imgThumb = null;
			private LinearLayout layout_list = null;
			
			
			public WrapperShowList(View row){
				this.row = row;
			}
			
			@SuppressLint("ResourceAsColor")
			private void init(final VideoHightlight vdo,final int pos){
			
				getTxtHeadline().setTypeface(type);
				getTxtdate().setTypeface(type);
				
				getTxtHeadline().setText(vdo.getCOL_headline());
				getTxtHeadline().setTextColor(Color.BLACK);
				getTxtdate().setText(vdo.getCOL_releaseDate());

				LinearLayout.LayoutParams flp= new LinearLayout.LayoutParams(180, 125);
				flp.setMargins(2, 2, 2, 2);
				getImgThumb().setLayoutParams(flp);
				
				imageLoader.displayImage(vdo.getCOL_thumbsImg(), getImgThumb(), options);
				
				getLayout_list().setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						getLayout_list().setBackgroundColor(Color.LTGRAY);
						
						GroupMainActivity.flag_intent=1;
						InVideo_Activity.position = pos;
						startActivity(new Intent(Video_Activity.this,GroupMainActivity.class));
						
//						insertDecorView(GroupMainActivity.mLocalActivityManager.startActivity("InVideo_Activity",new Intent(Video_Activity.this,InVideo_Activity.class)
//						.putExtra("position", pos)
//						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
//						.getDecorView()); GroupMainActivity.mLocalActivityManager.destroyActivity("", false);
					}
				});
			}
			
			private LinearLayout getLayout_list(){
				if(layout_list == null){
					layout_list = (LinearLayout)row.findViewById(R.id.layout_list);
				}
				return layout_list;
			}
			private TextView getTxtHeadline(){
				if(txtHeadline == null){
					txtHeadline = (TextView)row.findViewById(R.id.txt_headline);
				}
				return txtHeadline;
			}
			
			private TextView getTxtdate(){
				if(txtdate == null){
					txtdate = (TextView)row.findViewById(R.id.txt_date);
				}
				return txtdate;
			}
			private ImageView getImgThumb(){																		// SIGN OF DW IN SONG
				if(imgThumb == null){
					imgThumb = (ImageView)row.findViewById(R.id.img_thumb);
				}
				return imgThumb;
			}
			
	 }
	 
	 
	//******************************************************************************************************************************************************
	//********************************************************************SET MENU**************************************************************************
	//******************************************************************************************************************************************************
	
	private void setmenu() {
		listview_main_menu = (ListView) root.findViewById(R.id.listview_main_menu);
		btn_menu = (ImageView) root.findViewById(R.id.btn_menu);
		
		listview_main_menu.setAdapter(new menu_Adapter(this,1));

		btn_menu.setOnClickListener(this);
		listview_main_menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int pos,
					long arg3) {
				Log.i( "click :"+pos);
//				
		        switch (pos) {

				case 0: startActivity(new Intent(Video_Activity.this,News_Activity.class)); break;
				case 2: startActivity(new Intent(Video_Activity.this,Gallery_Activity.class)); break;
		        case 3: startActivity(new Intent(Video_Activity.this,Setting_Activity.class)); break;
		        case 4: startActivity(new Intent(Video_Activity.this,MainDownloaded.class)); break;
				}    
		      
				
			}
		});
	}

	private void insertDecorView(View view) {
		getParent().setContentView(
				view,
				new LayoutParams(LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT));
	}

	
	//******************************************************************************************************************************************************
		//************************************************************ SET CLICK / TOUCH ***********************************************************************
		//******************************************************************************************************************************************************
			
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_menu:
				root. toggleMenu();
				break;
			}
			
		}


		@Override
		public boolean onTouch(View v, MotionEvent event) {

			int MIN_DISTANCE = 100;
			if(downX==0) downX = event.getX();
			if(downY==0) downY = event.getY();
			
			 switch(event.getAction()){
			 
		        case MotionEvent.ACTION_UP: {
		            upX = event.getX();
		            upY = event.getY();

		            float deltaX = downX - upX;

		            if(Math.abs(deltaX) > MIN_DISTANCE){
		                // left or right
		                if(deltaX < 0 || deltaX > 0) { root. toggleMenu(); }
		            }
		           
		            return true;
		        }
		    }
		    return false;
		}
		

	//******************************************************************************************************************************************************
		//********************************************************************SET BANNER************************************************************************
		//******************************************************************************************************************************************************
				
		
		public class DownloadImageTask extends AsyncTask<String, Void, Void> {
			protected Void doInBackground(String... urls) {
				try {
					if(Unsteady.bannerBitmap.size()==0){
						Unsteady.bannerBitmap = new ArrayList<Bitmap>();
						if (Unsteady.List_BannerGroup.size() != 0) {
							for (BannerGroup item : Unsteady.List_BannerGroup) {
								for(Banner bann : item.getCOL_banner()){
									URL url = new URL(bann.getCOL_imgURL());
									
							        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
							        connection.setDoInput(true);
							        connection.connect();
							        InputStream input = connection.getInputStream();
							        Bitmap myBitmap = BitmapFactory.decodeStream(input);

									Unsteady.bannerBitmap.add(myBitmap);
								}
								
							}
						}else{
							Log.i("Banner : DownloadImageTask == Null");
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				if (Unsteady.bannerBitmap.size() != 0) {
					new BannerAdapter(Video_Activity.this, viewflipBanner);
				}
			}
		}
	
		//******************************************************************************************************************************************************
		//******************************************************************* EXIT *****************************************************************************
		//******************************************************************************************************************************************************

		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			// TODO Auto-generated method stub
			if (keyCode == KeyEvent.KEYCODE_BACK ){
				exit();
			}
			
			return super.onKeyDown(keyCode, event);
		}
		
		public void exit(){
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Exit ?");
		
			builder.setMessage("คุณต้องการออกจากแอพพลิเคชั่น ")
			       .setCancelable(true)
			       
			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
							Intent startMain = new Intent(
									Intent.ACTION_MAIN);
							startMain.addCategory(Intent.CATEGORY_HOME);
							startMain
									.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(startMain);
							onDestroy();
							android.os.Process
									.killProcess(android.os.Process.myPid());
			           }
			       })
			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			                dialog.cancel();
			           }
			       });
			 builder.show();
		}
		

	
}
