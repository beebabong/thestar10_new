package com.gmm.thestar10.menu_news;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.gmm.thestar10.R;
import com.gmm.thestar10.ApplicationConfig.GroupMainActivity;
import com.gmm.thestar10.ShareControl.ShareHelper;
import com.gmm.thestar10.Untilitys.Log;
import com.gmm.thestar10.Untilitys.Unsteady;
import com.gmm.thestar10.object.News;
import com.gmm.thestar10.object.NewsGallery;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class Photo_Of_News_Activity extends Activity {
	
	TextView txt_title;
	GridView grid_photoNews;
	ImageView btn_share;
	
	//Other
	 DisplayImageOptions options;
	 protected ImageLoader imageLoader = ImageLoader.getInstance();
	public static int position;
	 int width=0;
	 List<String> url_Gal = new ArrayList<String>();
	
		@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ac_photo_of_news);
		
		 options = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.img_profile)
			.showImageForEmptyUri(R.drawable.img_profile)
			.showImageOnFail(R.drawable.img_profile).cacheInMemory()
			.cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).build();
	        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
	        
		position = getIntent().getIntExtra("position", 0);
		mapping();
		setAction();
		
	}

	private void mapping() {
		txt_title = (TextView) findViewById(R.id.txt_title);
		grid_photoNews = (GridView) findViewById(R.id.grid_photoNews);
		btn_share = (ImageView) findViewById(R.id.btn_share);

		url_Gal = new ArrayList<String>();
	}

	private void setAction() {
		width = getWindowManager().getDefaultDisplay().getWidth();
		
		txt_title.setText(Unsteady.news.get(position).getCOL_title());
		grid_photoNews.setAdapter(new ImageAdapter(this));
		
		for(NewsGallery ng : Unsteady.news.get(position).getCOL_newsGallerys()){
			url_Gal.add(ng.getCOL_newsGalleryImgURL());	
		}
		
		grid_photoNews.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				Show_Gallery_Activity.url_Gal = url_Gal;
				Show_Gallery_Activity.position = pos;
				Log.i("gal news : "+ url_Gal.size() +"//"+Show_Gallery_Activity.url_Gal.size());
	        	
				insertDecorView(GroupMainActivity.mLocalActivityManager.startActivity("Show_Gallery_Activity_news",new Intent(Photo_Of_News_Activity.this,Show_Gallery_Activity.class).putExtra("comeform", "news")
				.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
				.getDecorView()); GroupMainActivity.mLocalActivityManager.destroyActivity("", false);
			}
		});
		
		btn_share.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String URLForShare = "";
				String word = Unsteady.news.get(position).getCOL_title();		
				
				new ShareHelper(Photo_Of_News_Activity.this,word,Unsteady.news.get(position).getCOL_cover(),URLForShare);
			}
		});
		
	}
	
	private void insertDecorView(View view) {
		getParent().setContentView(
				view,
				new LayoutParams(LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT));
	}
	



	//******************************************************************************************************************************************************
	//***************************************************************** SET GRIDVIEW************************************************************************
	//******************************************************************************************************************************************************
	
	class ImageAdapter extends BaseAdapter {
        private Context mcontext;
        public ImageAdapter(Context c)
        {
        	mcontext=c;
        }

        @Override
        public int getCount() {
        	return Unsteady.news.get(position).getCOL_newsGallerys().size();
        }

        @Override
        public Object getItem(int position) {
        	return null;
        }

        @Override
        public long getItemId(int position) {
        	return 0;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
        	ImageView imageView = new ImageView(mcontext);
          imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
          GridView.LayoutParams flpgal = new GridView.LayoutParams(width/2,width/2);
          imageView.setLayoutParams(flpgal);
                        
        	Log.i("link news : "+ Unsteady.news.get(position).getCOL_newsGallerys().size()+"//"+Unsteady.news.get(position).getCOL_newsGallerys().get(pos).getCOL_newsGalleryImgURL());
        	imageLoader.displayImage(Unsteady.news.get(position).getCOL_newsGallerys().get(pos).getCOL_newsGalleryImgURL(), imageView, options);
        		
            return imageView;
        }



}
}
