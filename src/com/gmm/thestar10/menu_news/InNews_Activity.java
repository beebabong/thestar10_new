package com.gmm.thestar10.menu_news;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.gmm.thestar10.R;
import com.gmm.thestar10.ApplicationConfig.GroupMainActivity;
import com.gmm.thestar10.ShareControl.ShareHelper;
import com.gmm.thestar10.Untilitys.Unsteady;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class InNews_Activity extends Activity {
	
	//Activity in this
		ImageView img_thumbnail,btn_share;
		ImageView[] img_galnews;
		TextView txt_headline,txt_datepost,txt_news;
		LinearLayout gal_news;
		
	//Other
		 DisplayImageOptions options;
		 protected ImageLoader imageLoader = ImageLoader.getInstance();
		 public static int position;
		 int[] resourceImg = { R.id.img_galnews1,R.id.img_galnews2,R.id.img_galnews3,R.id.img_galnews4};
		 static ArrayList<Bitmap> newsgal_Bitmap = new ArrayList<Bitmap>();
		 int i=0;
		 Typeface type;
			
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ac_innews);
		
		 options = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.img_profile)
			.showImageForEmptyUri(R.drawable.img_profile)
			.showImageOnFail(R.drawable.img_profile).cacheInMemory()
			.cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).build();
	        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
	    type=Typeface.createFromAsset(getAssets(),"THSarabun_Bold.ttf");
//	    position = getIntent().getIntExtra("position", 0);
		mapping();
		setAction();
	}

	private void setAction() {
		int width = getWindowManager().getDefaultDisplay().getWidth();
		
		LinearLayout.LayoutParams flp= new LinearLayout.LayoutParams(width, 400);
		flp.setMargins(2, 2, 2, 2);
		img_thumbnail.setLayoutParams(flp);
		imageLoader.displayImage(Unsteady.news.get(position).getCOL_thmImg(), img_thumbnail, options);


		LinearLayout.LayoutParams[] flpgal = new LinearLayout.LayoutParams[4];
		for(int i=0;i<4;++i){
			flpgal[i] = new LinearLayout.LayoutParams(width/4, width/4);
			flpgal[i].setMargins(1, 1, 1, 1);
			img_galnews[i] = (ImageView) findViewById(resourceImg[i]);
			
			if(Unsteady.news.get(position).getCOL_newsGallerys().size()>i){
				img_galnews[i].setLayoutParams(flpgal[i]);
				
				imageLoader.displayImage(Unsteady.news.get(position).getCOL_newsGallerys().get(i).getCOL_newsGalleryImgURL(), img_galnews[i], options);

			}else img_galnews[i].setVisibility(View.INVISIBLE);
				
			
		}
		
		
		txt_headline.setText(Unsteady.news.get(position).getCOL_title());
		txt_datepost.setText("Posted "+Unsteady.news.get(position).getCOL_releaseDate());
		txt_news.setText(Html.fromHtml(Unsteady.news.get(position).getCOL_desc()));
		
		gal_news.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View arg0) {
					insertDecorView(GroupMainActivity.mLocalActivityManager.startActivity("Photo_Of_News_Activity",new Intent(InNews_Activity.this,Photo_Of_News_Activity.class)
					.putExtra("position", position)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
					.getDecorView()); GroupMainActivity.mLocalActivityManager.destroyActivity("", false);
				
			}
		});
		
		btn_share.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String URLForShare = "";
				String word = Unsteady.news.get(position).getCOL_title();		
				
				new ShareHelper(InNews_Activity.this,word,Unsteady.news.get(position).getCOL_cover(),URLForShare);
			}
		});
		
	}

	private void mapping() {
		img_thumbnail = (ImageView) findViewById(R.id.img_thumbnail);
		img_galnews = new ImageView[4];
		
		txt_headline= (TextView) findViewById(R.id.txt_headline);
		txt_datepost= (TextView) findViewById(R.id.txt_datepost);
		txt_news= (TextView) findViewById(R.id.txt_news);
		
		btn_share = (ImageView) findViewById(R.id.btn_share);
		
		gal_news = (LinearLayout) findViewById(R.id.gal_news);
		
		txt_headline.setTypeface(type);
		txt_datepost.setTypeface(type);
		txt_news.setTypeface(type);
	}

	
	private void insertDecorView(View view) {
		getParent().setContentView(
				view,
				new LayoutParams(LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT));
	}
	
}
